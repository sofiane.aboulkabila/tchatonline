import Joi from 'joi';

const chargeValidationSchema = Joi.object({
    amount: Joi.number().integer().min(50),
    currency: Joi.string().lowercase().length(3).default('eur'),
    source: Joi.string(),
    description: Joi.string().max(255).allow('')
}).options({ allowUnknown: true, presence: 'optional' });

export const validateStripe = (req, res, next) => {
    const { error } = chargeValidationSchema.validate(req.body);
    if (error) {
        return res.status(400).json({
            success: false,
            message: 'Invalid request data: ' + error.details.map(detail => detail.message).join(', ')
        });
    }
    next();
};
