import Joi from 'joi';

const orderValidationSchema = Joi.object({
    product: Joi.string(),
    user: Joi.string(),
    status: Joi.string().valid('pending', 'shipped', 'delivered', 'cancelled').default('pending'),
    deliveryAddress: Joi.string()
}).options({ allowUnknown: true, presence: 'optional' });

export const validateOrder = (req, res, next) => {
    const { error } = orderValidationSchema.validate(req.body);

    if (error) {
        return res.status(400).json({ error: 'Invalid fields: ' + error.details.map(detail => detail.message).join(', ') });
    }

    next();
};
