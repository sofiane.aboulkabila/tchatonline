import Joi from 'joi';

const chatRoomValidationSchema = Joi.object({
    name: Joi.string().min(3).max(50),
    users: Joi.array().items(Joi.string()),
    messages: Joi.array().items(Joi.string())
}).options({ allowUnknown: true, presence: 'optional' });

export const validateChatRoom = (req, res, next) => {
    const { error } = chatRoomValidationSchema.validate(req.body);

    if (error) {
        return res.status(400).json({ error: 'Invalid chat room data: ' + error.details.map(detail => detail.message).join(', ') });
    }

    next();
};
