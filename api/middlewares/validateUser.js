import Joi from 'joi';

const roles = ['Utilisateur', 'Moderateur', 'Administrateur'];

const userValidationSchema = Joi.object({
    username: Joi.string().min(3).max(20),
    email: Joi.string().email().min(5).max(50),
    password: Joi.string().min(8).max(100),
    role: Joi.string().valid(...roles),
    image: Joi.string().default('default.png').allow(null, '')
}).options({ allowUnknown: true, presence: 'optional' });

export const validateUser = (req, res, next) => {
    const { error } = userValidationSchema.validate(req.body);

    if (error) {
        return res.status(400).json({ error: 'Champs invalides: ' + error.details.map(detail => detail.message).join(', ') });
    }

    next();
};
