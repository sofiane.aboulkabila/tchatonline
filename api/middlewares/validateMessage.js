import Joi from 'joi';

const messageValidationSchema = Joi.object({
    content: Joi.string().trim().min(1).max(500),
    author: Joi.string().trim()
}).options({ allowUnknown: true, presence: 'optional' });

export const validateMessage = (req, res, next) => {
    const { error } = messageValidationSchema.validate(req.body);

    if (error) {
        return res.status(400).json({ error: 'Champs invalides: ' + error.details.map(detail => detail.message).join(', ') });
    }

    next();
};
