import Joi from 'joi';

const productValidationSchema = Joi.object({
    name: Joi.string().trim().min(3).max(50),
    description: Joi.string().trim().max(500),
    price: Joi.number().min(0).positive(),
    quantity: Joi.number().min(0),
    category: Joi.string(),
    image: Joi.string(),
}).options({ allowUnknown: true, presence: 'optional' });

export const validateProduct = (req, res, next) => {
    const { error } = productValidationSchema.validate(req.body);

    if (error) {
        return res.status(400).json({ error: 'Champs invalides: ' + error.details.map(detail => detail.message).join(', ') });
    }

    next();
};
