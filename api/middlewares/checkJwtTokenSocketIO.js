import jwt from 'jsonwebtoken';

export const checkJwtTokenSocketIO = (socket, next) => {
    const token = socket.handshake.headers['authorization'].split(' ')[1];

    if (!token) {
        return next(new Error('Accès non autorisé. Aucun jeton fourni.'));
    }

    try {
        const decodedToken = jwt.verify(token, process.env.JWT_SECRET);
        socket.user = decodedToken;
        next();
    } catch (error) {
        console.log(error)
        return next(new Error('Accès non autorisé. Jeton invalide.'));
    }
};
