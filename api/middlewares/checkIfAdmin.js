import { UserModel } from '../models/userModel';

export const checkAdminRole = async (req, res, next) => {
    const userId = req.user.id;

    try {
        const user = await UserModel.findById(userId);

        if (user && user.role === 'Administrateur') {
            next();
        } else {
            res.status(403).json({ message: "Accès refusé. Vous n'avez pas les droits d'administrateur nécessaires." });
        }
    } catch (error) {
        res.status(500).json({ message: "Erreur lors de la vérification du rôle de l'utilisateur." });
    }
};
