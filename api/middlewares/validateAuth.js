import Joi from 'joi';

export const validateAuth = (req, res, next) => {
    let schema;
    if (req.path === "/register") {

        schema = Joi.object({
            username: Joi.string().min(3).max(20),
            email: Joi.string().email().min(5).max(50),
            password: Joi.string().min(8).max(100),
            role: Joi.string().valid(...['Utilisateur', 'Moderateur', 'Administrateur']),
            image: Joi.string().default('default.png')
        }).options({ allowUnknown: true, presence: 'optional' });

    } else if (req.path === "/login") {

        schema = Joi.object({
            username: Joi.string().min(3).max(20),
            password: Joi.string().min(8).max(100)
        }).options({ allowUnknown: true, presence: 'optional' });

    } else {
        return res.status(400).json({ error: 'Requête non supportée' });
    }

    const { error } = schema.validate(req.body);
    if (error) {
        return res.status(400).json({ error: error.details[0].message });
    }

    next();
};
