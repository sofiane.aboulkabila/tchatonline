import Joi from 'joi';
import jwt from 'jsonwebtoken';

export const checkJwtToken = (req, res, next) => {
  const tokenSchema = Joi.string().pattern(/^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.[A-Za-z0-9-_.+/=]*$/)
    .options({ allowUnknown: true, presence: 'optional' });

  const token = req.header('Authorization').split(' ')[1];
  const { error } = tokenSchema.validate(token);
  if (error) {
    return res.json({ message: 'Accès non autorisé. Format de jeton invalide.' });
  }

  try {
    const decodedToken = jwt.verify(token, process.env.JWT_SECRET);
    req.user = decodedToken;

    next();
  } catch (error) {
    return res.json({ message: 'Accès non autorisé. Jeton invalide.' });
  }
};
