import { Schema, model } from 'mongoose';

const chatRoomSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    messages: [{
        type: Schema.Types.ObjectId,
        ref: 'Message'
    }]
});

export const ChatRoomModel = model('ChatRoom', chatRoomSchema);
