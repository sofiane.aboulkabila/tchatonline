import { Schema, model } from 'mongoose';

const roles = ['Utilisateur', 'Moderateur', 'Administrateur'];

const userSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique: true,
        minlength: 3,
        maxlength: 20
    },
    email: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
    },
    password: {
        type: String,
        required: true,
        minlength: 8,
        maxlength: 100
    },
    role: {
        type: String,
        enum: roles,
        required: true
    },
    image: {
        type: String,
        default: 'default.png',
    },
    color: {
        type: String,
    }
}, { timestamps: true });

export const UserModel = model('User', userSchema);
