import axios from 'axios';
import Chance from 'chance';
import dotenv from 'dotenv';
import path from 'path';
import { fileURLToPath } from 'url';

dotenv.config({ path: path.resolve(path.dirname(fileURLToPath(import.meta.url)), '..', '.env') });

const chance = new Chance();

async function createColorProduct() {
    try {
        const colorProductData = {
            name: 'color',
            description: 'Permet de changer la couleur du pseudo dans le tchat',
            price: 5,
            quantity: 1000,
            category: 'Others'
        };

        const response = await axios.post("http://localhost:8000/api/products", colorProductData, {
            headers: {
                Authorization: process.env.JWT_TOKEN
            }
        });
        console.log('Produit "color" créé avec succès :', response.data);
    } catch (erreur) {
        console.error('Erreur lors de la création du produit "color" :', erreur.response ? erreur.response.data : erreur.message);
    }
}

async function generateProducts() {
    try {
        await createColorProduct();

        const categories = ['Electronics', 'Clothing', 'Books', 'Home', 'Others'];

        for (let count = 1; count <= 100; count++) {
            const productData = {
                name: `product-${count}`,
                description: chance.sentence({ words: 12 }),
                price: chance.floating({ min: 10, max: 1000, fixed: 2 }),
                quantity: chance.integer({ min: 10, max: 100 }),
                category: chance.pickone(categories),
            };

            const response = await axios.post("http://localhost:8000/api/products", productData, {
                headers: {
                    Authorization: process.env.JWT_TOKEN
                }
            });
            console.log('Product created successfully:', response.data);
        }
    } catch (error) {
        console.error('Error creating products:', error.response ? error.response.data : error.message);
    }
}

generateProducts();
