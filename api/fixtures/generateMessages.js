import axios from 'axios';
import Chance from 'chance';
import dotenv from 'dotenv';
import path from 'path';
import { fileURLToPath } from 'url';

dotenv.config({ path: path.resolve(path.dirname(fileURLToPath(import.meta.url)), '..', '.env') });

const chance = new Chance();

function getRandomUserId(users) {
    const randomIndex = Math.floor(Math.random() * users.length);
    return users[randomIndex]._id;
}

async function generateMessages() {
    try {
        const usersResponse = await axios.get("http://localhost:8000/api/users", {
            headers: {
                Authorization: process.env.JWT_TOKEN
            }
        });
        const users = usersResponse.data;

        const messagesResponse = await axios.get("http://localhost:8000/api/messages", {
            headers: {
                Authorization: process.env.JWT_TOKEN
            }
        });
        const totalCount = messagesResponse.data.length;

        for (let count = totalCount + 1; count <= totalCount + 100; count++) {
            const content = `${count}. ${chance.paragraph({ sentences: 1, min: 1, max: 500 })}`;
            const author = getRandomUserId(users);

            const messageData = {
                content,
                author,
            };

            const response = await axios.post("http://localhost:8000/api/messages", messageData, {
                headers: {
                    Authorization: process.env.JWT_TOKEN
                }
            });

            console.log('Message créé avec succès :', response.data);
        }
    } catch (error) {
        console.error('Erreur lors de la création des messages :', error.response ? error.response.data : error.message);
    }
}

generateMessages();

