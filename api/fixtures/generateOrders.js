import axios from 'axios';
import Chance from 'chance';
import dotenv from 'dotenv';
import path from 'path';
import { fileURLToPath } from 'url';

dotenv.config({ path: path.resolve(path.dirname(fileURLToPath(import.meta.url)), '..', '.env') });

const chance = new Chance();

async function getRandomUser() {
    try {
        const response = await axios.get("http://localhost:8000/api/users", {
            headers: {
                Authorization: process.env.JWT_TOKEN
            }
        });
        const users = response.data;

        return chance.pickone(users);
    } catch (error) {
        console.error('Erreur lors de la récupération d\'un utilisateur :', error.response ? error.response.data : error.message);
        return null;
    }
}

async function getRandomProduct() {
    try {
        const response = await axios.get("http://localhost:8000/api/products", {
            headers: {
                Authorization: process.env.JWT_TOKEN
            }
        });
        const products = response.data;
        return chance.pickone(products);
    } catch (error) {
        console.error('Erreur lors de la récupération des produits :', error.response ? error.response.data : error.message);
        return null;
    }
}

async function generateOrder() {
    try {
        const statuses = ['pending', 'shipped', 'delivered', 'cancelled'];

        const user = await getRandomUser();
        const product = await getRandomProduct();

        if (!user || !product) {
            console.error('Impossible de générer la commande. Utilisateur ou produit non disponible.');
            return;
        }

        const orderData = {
            product: product._id,
            user: user._id,
            status: chance.pickone(statuses),
            deliveryAddress: chance.address()
        };

        const response = await axios.post("http://localhost:8000/api/orders", orderData, {
            headers: {
                Authorization: process.env.JWT_TOKEN
            }
        });

        console.log('Commande créée avec succès :', response.data);
    } catch (error) {
        console.error('Erreur lors de la création de la commande :', error.response ? error.response.data : error.message);
    }
}

async function generateOrders() {
    try {
        for (let i = 0; i < 50; i++) {
            console.log(`Génération de la commande ${i + 1}...`);
            await generateOrder();
        }
    } catch (error) {
        console.error('Erreur lors de la génération des commandes :', error.response ? error.response.data : error.message);
    }
}

generateOrders();
