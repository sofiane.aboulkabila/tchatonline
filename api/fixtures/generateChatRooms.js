import axios from 'axios';
import dotenv from 'dotenv';
import path from 'path';
import { fileURLToPath } from 'url';

dotenv.config({ path: path.resolve(path.dirname(fileURLToPath(import.meta.url)), '..', '.env') });

async function getMessages() {
    try {
        const response = await axios.get("http://localhost:8000/api/messages", {
            headers: {
                Authorization: process.env.JWT_TOKEN
            }
        });
        return response.data;
    } catch (error) {
        console.error('Erreur lors de la récupération des messages :', error.response ? error.response.data : error.message);
        return [];
    }
}

async function sendRoomData(roomData) {
    const url = 'http://localhost:8000/api/chatrooms';
    try {
        const response = await axios.post(url, roomData, {
            headers: {
                Authorization: process.env.JWT_TOKEN
            }
        });
        console.log('Chat room created successfully:', response.data);
    } catch (error) {
        console.error('Error creating chat room:', error.response ? error.response.data : error.message);
    }
}

async function generateChatRooms() {
    try {
        const generalRoomData = {
            name: 'general',
            messages: []
        };
        await sendRoomData(generalRoomData);

        const messages = await getMessages();
        for (let count = 1; count <= 20; count++) {
            const roomData = {
                name: `ChatRoom-${count}`,
                messages: messages.map(message => message._id).slice(0, 10)
            };
            await sendRoomData(roomData);
        }
    } catch (error) {
        console.error('Error generating chat rooms:', error.response ? error.response.data : error.message);
    }
}

generateChatRooms();
