
import axios from 'axios';

import dotenv from 'dotenv';
import path from 'path';
import { fileURLToPath } from 'url';

dotenv.config({ path: path.resolve(path.dirname(fileURLToPath(import.meta.url)), '..', '.env') });



function generateEmail(username, maxLength) {
    const domain = "@test.fr";
    const emailBody = `${username}`.substring(0, maxLength - domain.length);
    return `${emailBody}${domain}`.toLowerCase();
}

async function generateUsers() {
    const url = 'http://localhost:8000/api/register';

    for (let count = 1; count <= 100; count++) {
        const username = `user-${count}`;
        const userData = {
            username: username,
            email: generateEmail(username, 50),
            password: 'test12345',
            role: count === 1 ? 'Administrateur' : (count <= 5 ? 'Moderateur' : 'Utilisateur'),
            image: 'default.png',
        };

        try {
            const response = await axios.post(url, userData, {
                headers: {
                    Authorization: process.env.JWT_TOKEN
                }
            });
            console.log('User created successfully:', response.data);
        } catch (error) {
            console.error('Error creating user:', error.response ? error.response.data : error.message);
        }
    }
}

generateUsers();

export default generateUsers;
