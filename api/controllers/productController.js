import { ProductModel } from '../models/productModel.js';

export const getProducts = async (req, res) => {
    try {
        const products = await ProductModel.find();
        res.status(200).json(products);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

export const getProductById = async (req, res) => {
    try {
        const id = req.params.id;

        const message = await ProductModel.findById(id);

        if (!message) return res.status(404).json({ message: 'Message non trouvé.' });

        return res.status(200).json(message);
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

export const createProduct = async (req, res) => {
    const product = req.body;

    try {
        const newProduct = await ProductModel.create(product);

        res.status(201).json(newProduct);
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
};

export const deleteProduct = async (req, res) => {
    const { id } = req.params;

    try {
        const deletedProduct = await ProductModel.findByIdAndDelete(id);

        if (!deletedProduct) throw Error('ProductModel not found');
        res.status(200).json({ message: 'Produit supprimé avec succès' });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
};
