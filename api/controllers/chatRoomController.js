import { ChatRoomModel } from '../models/chatRoomModel.js';

export const createChatRoom = async (req, res) => {
    try {
        const chatRoom = new ChatRoomModel(req.body);
        await chatRoom.save();

        res.json(chatRoom);


    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

export const getAllChatRooms = async (req, res) => {
    try {
        const chatRooms = await ChatRoomModel.find();
        res.json(chatRooms);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

export const updateChatRoom = async (req, res) => {
    try {
        const chatRoom = await ChatRoomModel.findByIdAndUpdate(req.params.id, req.body, { new: true });

        if (!chatRoom) {
            return res.status(404).json({ message: 'Chat room not found.' });
        }

        res.json(chatRoom);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

export const deleteChatRoom = async (req, res) => {
    try {
        const chatRoom = await ChatRoomModel.findByIdAndDelete(req.params.id);

        if (!chatRoom) {
            return res.status(404).json({ message: 'Chat room not found.' });
        }

        res.json({ message: 'Chat room deleted.' });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};