import { Stripe } from 'stripe';
import { UserModel } from '../models/userModel.js';

const buildRelevantChargeInfo = (charge) => ({
    id: charge.id,
    amount: charge.amount,
    currency: charge.currency,
    status: charge.status,
    description: charge.description,
    paid: charge.paid,
    receipt_url: charge.receipt_url
});

export const createCharge = async (req, res) => {
    try {
        const stripe = new Stripe(process.env.STRIPE_SECRET_KEY);

        const { amount, source, currency = 'eur', name } = req.body;
        const id = req.params.id;

        const charge = await stripe.charges.create({
            amount,
            currency,
            source,
            description: 'Charge for product'
        });

        if (name === "color") {
            await UserModel.updateOne({ _id: id }, { color: 'paid' });
        }
        res.json({ success: true, ...buildRelevantChargeInfo(charge) });
    } catch (error) {
        console.error("Stripe error caught:", error);
        if (error.type === 'StripeCardError') {
            res.status(400).json({ success: false, message: "Card error: " + error.message });
        } else {
            res.status(500).json({ success: false, message: "Stripe error: " + error.message });
        }
    }
};

export const listAllCharges = async (req, res) => {
    try {
        const stripe = new Stripe(process.env.STRIPE_SECRET_KEY);
        const charges = await stripe.charges.list({
            limit: 10
        });
        const chargesInfo = charges.data.map(buildRelevantChargeInfo);
        res.json({ success: true, ...chargesInfo });
    } catch (error) {
        console.error("Error retrieving charges:", error);
        res.status(500).json({ success: false, message: "Error retrieving charges: " + error.message });
    }
};

export const retrieveCharge = async (req, res) => {
    try {
        const stripe = new Stripe(process.env.STRIPE_SECRET_KEY);
        const id = req.params.id;

        const charge = await stripe.charges.retrieve(id);
        const chargeInfo = buildRelevantChargeInfo(charge);
        res.json({ success: true, ...chargeInfo });
    } catch (error) {
        console.error("Error retrieving charge:", error);
        res.status(500).json({ success: false, message: "Error retrieving charge: " + error.message });
    }
};
