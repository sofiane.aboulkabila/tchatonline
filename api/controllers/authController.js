import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import { UserModel } from '../models/userModel.js';
import { encryptFile } from '../utils/encryptFiles.js';

export async function registerUser(req, res) {
    try {
        let encryptedFilePath;
        if (req.file) {
            encryptedFilePath = await encryptFile(req.file.path);
        }

        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(req.body.password, salt);

        const newUser = await UserModel.create({
            ...req.body,
            password: hashedPassword,
            image: encryptedFilePath
        });

        return res.json(newUser);
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

export async function loginUser(req, res) {
    const { username, password } = req.body;

    try {
        const user = await UserModel.findOne({ username });

        if (!user) {
            return res.status(401).json({ message: 'Nom de compte ou mot de passe incorrect' });
        }

        const passwordMatch = await bcrypt.compare(password, user.password);

        if (!passwordMatch) {
            return res.status(401).json({ message: 'Nom de compte ou mot de passe incorrect' });
        }

        const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET, { expiresIn: '30d' });

        return res.status(200).json({
            message: 'Connexion réussie',
            token,
            tokenType: 'Bearer',
            _id: user._id,
            username: user.username,
            role: user.role,
            email: user.email,
            color: user.color
        });
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}
