import { MessageModel } from '../models/messageModel.js';

export async function getMessages(req, res) {
    try {
        const messages = await MessageModel.find();

        if (messages.length === 0) return res.status(200).json({ message: 'Aucun message trouvé.' });

        return res.json(messages);
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

export async function getMessageById(req, res) {
    try {
        const id = req.params.id;

        const message = await MessageModel.findById(id);

        if (!message) return res.status(404).json({ message: 'MessageModel non trouvé.' });

        return res.status(200).json(message);
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

export async function createMessage(req, res) {
    try {
        const { content, author } = req.body;
        const newMessageData = {
            content,
            author
        };

        const newMessage = await MessageModel.create(newMessageData);

        return res.status(200).json({ message: 'MessageModel créé.', _id: newMessage._id });
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

export async function deleteMessage(req, res) {
    try {
        const id = req.params._id;

        const deletedMessage = await MessageModel.findByIdAndDelete(id);

        if (!deletedMessage) return res.json({ message: 'MessageModel non trouvé.' });

        return res.json({ message: 'MessageModel supprimé.' });
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}
