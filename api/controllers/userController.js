import { UserModel } from '../models/userModel.js';
import bcrypt from 'bcrypt';


export async function getUsers(req, res) {
    try {
        const users = await UserModel.find();

        if (users.length === 0) return res.json({ message: 'Aucun utilisateur existant.' });

        return res.json(users);
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

export async function editUsername(req, res) {
    try {
        const { id } = req.params;
        const { username, password } = req.body;

        const user = await UserModel.findById(id);
        if (!user) {
            return res.status(404).json({ message: 'Utilisateur non trouvé.' });
        }

        const passwordMatch = await bcrypt.compare(password, user.password);
        if (!passwordMatch) {
            return res.status(401).json({ message: 'Mot de passe incorrect.' });
        }

        const updatedUser = await UserModel.findByIdAndUpdate(id, { username }, { new: true });

        return res.json({ message: 'Nom d\'utilisateur mis à jour.', user: updatedUser });
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

export async function editUser(req, res) {
    try {
        const { id } = req.params;
        const userData = req.body;

        const user = await UserModel.findById(id);

        if (!user) {
            return res.status(404).json({ message: 'Utilisateur non trouvé.' });
        }

        const updatedUser = await UserModel.findByIdAndUpdate(id, userData, { new: true });

        return res.json({ message: 'Utilisateur mis à jour.', user: updatedUser });
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

export async function getUserById(req, res) {
    try {
        const userId = req.params.id;
        const user = await UserModel.findById(userId);

        if (!user) {
            return res.status(404).json({ message: 'Utilisateur non trouvé.' });
        }

        return res.json(user);
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

export async function editPassword(req, res) {
    try {
        const { id } = req.params;
        const { old_password, new_password } = req.body;

        const user = await UserModel.findById(id);
        if (!user) {
            return res.status(404).json({ message: 'Utilisateur non trouvé.' });
        }

        const passwordMatch = await bcrypt.compare(old_password, user.password);
        if (!passwordMatch) {
            return res.status(401).json({ message: 'Ancien mot de passe incorrect.' });
        }

        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(new_password, salt);

        await UserModel.findByIdAndUpdate(id, { password: hashedPassword });

        return res.json({ message: 'Mot de passe mis à jour avec succès.' });
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

export async function editColor(req, res) {
    try {
        const { id } = req.params;
        const { color, password } = req.body;

        const user = await UserModel.findById(id);
        if (!user) {
            return res.status(404).json({ message: 'Utilisateur non trouvé.' });
        }

        const passwordMatch = await bcrypt.compare(password, user.password);
        if (!passwordMatch) {
            return res.status(401).json({ message: 'Mot de passe incorrect.' });
        }

        const updatedUser = await UserModel.findByIdAndUpdate(id, { color }, { new: true });

        return res.json({ message: 'Couleur du pseudo mis à jour.', user: updatedUser });
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}


export async function editImage(req, res) {
    try {
        const { id } = req.params;
        const { image, password } = req.body;

        const user = await UserModel.findById(id);
        if (!user) {
            return res.status(404).json({ message: 'Utilisateur non trouvé.' });
        }

        const passwordMatch = await bcrypt.compare(password, user.password);
        if (!passwordMatch) {
            return res.status(401).json({ message: 'Mot de passe incorrect.' });
        }

        const updatedUser = await UserModel.findByIdAndUpdate(id, { image }, { new: true });

        return res.json({ message: 'Image de profile mise à jour avec succès.', user: updatedUser });
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

export async function deleteUser(req, res) {
    try {
        const id = req.params.id;
        const deletedUser = await UserModel.findByIdAndDelete(id);

        if (!deletedUser) return res.json({ message: 'Utilisateur non existant.' });

        return res.json({ message: 'Utilisateur supprimé.' });
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}
