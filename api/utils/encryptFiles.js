import { randomBytes, createCipheriv } from 'crypto';
import fs from 'fs';

export async function encryptFile(filePath) {
    const algorithm = 'aes-256-ctr';
    const key = randomBytes(32);
    const iv = randomBytes(16);

    const cipher = createCipheriv(algorithm, key, iv);
    const input = fs.createReadStream(filePath);
    const encryptedFilePath = `${filePath}.enc`;

    const output = fs.createWriteStream(encryptedFilePath);

    return new Promise((resolve, reject) => {
        input.pipe(cipher).pipe(output);
        output.on('finish', () => {
            fs.unlink(filePath, (err) => { if (err) console.log("Error deleting original file:", err); });
            resolve(encryptedFilePath);
        });
        output.on('error', reject);
    });
}
