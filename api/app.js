import express from 'express';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import helmet from "helmet";
import cors from 'cors';

import { createServer } from 'http';
import { Server as SocketIOServer } from 'socket.io';
import { setupWebSocketIo } from './socketIO/main.js';

import { checkJwtToken } from './middlewares/checkJwtToken.js';
import { authRouter } from './routes/authRouter.js';
import { userRouter } from './routes/userRouter.js';
import { chatRoomRouter } from './routes/chatRoomRouter.js';
import { messageRouter } from './routes/messageRouter.js';
import { productRouter } from './routes/productRouter.js';
import { stripeRouter } from './routes/stripeRouter.js';
import { orderRouter } from './routes/orderRouter.js';

dotenv.config();

const { API_PORT, TEST_PORT, ENV, MONGO_DEV_URI, MONGO_PROD_URI } = process.env;

const app = express();

const httpServer = createServer(app);

const io = new SocketIOServer(httpServer, {
    cors: {
        origin: "http://localhost:5173",
        methods: ["GET", "POST"],
        allowedHeaders: ["Authorization"],
        credentials: true
    }
});

app.use(cors({
    origin: ['http://localhost:5173']
}));

app.use(helmet());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get('/', (req, res) => { res.json("connected to vercel") });
app.use('/img/users', express.static('img/users'));

app.use('/api', authRouter);

app.use('/api',
    productRouter,
);

app.use('/api', checkJwtToken,
    userRouter,
    chatRoomRouter,
    messageRouter,
    productRouter,
    stripeRouter,
    orderRouter
);


setupWebSocketIo(io);

let uri;

switch (ENV) {
    case 'development':
        uri = MONGO_DEV_URI;
        console.log('Connexion à MongoDB en développement réussie ✅ !');
        break;
    case 'production':
        uri = MONGO_PROD_URI;
        console.log('Connexion à MongoDB en production réussie ✅ !');
        break;
    default:
        console.error(`Environnement "${ENV}" non supporté.`);
        process.exit(1);
}

mongoose.connect(uri);

const startServer = () => {
    httpServer.listen(API_PORT, () => {
        console.log(`Serveur API et WebSocket démarré avec succès sur le port ${API_PORT} ! 🚀✅`);
    })
        .on('error', (err) => {
            if (err.code === 'EADDRINUSE') {
                console.error(`Le port ${API_PORT} est déjà utilisé, tentative de démarrage sur le port ${TEST_PORT}...`);

                httpServer.listen(TEST_PORT, () => {
                    console.log(`Serveur démarré avec succès sur le port ${TEST_PORT} ! ✅`);
                })
                    .on('error', err => {
                        console.error('Erreur lors du démarrage du serveur sur le port de secours:', err);
                        process.exit(1);
                    });
            } else {
                console.error('Erreur lors du démarrage du serveur:', err);
                process.exit(1);
            }
        });
};

startServer();

export default app;
