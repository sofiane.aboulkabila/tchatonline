import { checkJwtTokenSocketIO } from '../middlewares/checkJwtTokenSocketIO.js';
import { MessageModel } from '../models/messageModel.js';

export const setupWebSocketIo = (io) => {
    const connectedUsers = {};

    io.on('connection', socket => {

        checkJwtTokenSocketIO(socket, (error) => {
            if (error) { return socket.disconnect() }

            socket.on('send-nickname', (username, color, userId) => {
                connectedUsers[socket.id] = { username, socketId: socket.id, color, userId };

                io.emit('connected_users', Object.values(connectedUsers));
                socket.join("general");

                socket.emit("receive_message", {
                    sender: 'Système',
                    content: `Bonjour ${username}, bienvenue dans le salon général.`,
                    private: true,
                    color: 'red',
                    senderId: 'system'
                });
            });

            socket.on('disconnect', () => {
                delete connectedUsers[socket.id];
                io.emit('connected_users', Object.values(connectedUsers));
            });

            socket.on("join_room", (room) => {
                const oldRoom = Array.from(socket.rooms).find(r => r !== socket.id && r !== room);
                if (oldRoom) {
                    socket.leave(oldRoom);
                }
                socket.join(room);
                if (room !== oldRoom) {
                    socket.emit("receive_message", {
                        sender: 'Système',
                        content: `${connectedUsers[socket.id].username} a rejoint le salon ${room}.`,
                        private: true
                    });
                }
            });

            socket.on("send_message", (data) => {
                const { username, color, userId } = connectedUsers[socket.id];
                const message = { sender: username, content: data.content, color, senderId: userId };
                io.to(data.room).emit("receive_message", message);
            });

            socket.on("send_private_message", (data) => {
                const { username, userId } = connectedUsers[socket.id];
                const recipientSocketId = Object.keys(connectedUsers).find(key => connectedUsers[key].username === data.recipient);
                if (recipientSocketId) {
                    const message = { sender: username, content: data.content, private: true, senderId: userId };
                    io.to(recipientSocketId).emit("receive_message", message);
                } else {
                    io.to(socket.id).emit("receive_message", {
                        sender: 'Système',
                        content: `L'utilisateur ${data.recipient} n'a pas été trouvé.`,
                        private: true,
                        senderId: 'system'
                    });
                }
            });
        });
    });
};
