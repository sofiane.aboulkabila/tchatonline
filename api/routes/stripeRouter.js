import express from 'express';
import * as stripeController from '../controllers/stripeController.js';
import { validateStripe } from '../middlewares/validateStripe.js';

export const stripeRouter = express.Router();

stripeRouter.route('/stripe/charges')
    .get(validateStripe, stripeController.listAllCharges);

stripeRouter.route('/stripe/charges/:id')
    .get(validateStripe, stripeController.retrieveCharge)
    .post(validateStripe, stripeController.createCharge);