import express from 'express';
import { validateMessage } from '../middlewares/validateMessage.js';
import * as messageController from '../controllers/messageController.js';

export const messageRouter = express.Router();

messageRouter.route('/messages')
    .get(validateMessage, messageController.getMessages)
    .post(validateMessage, messageController.createMessage);

messageRouter.route('/messages/:id')
    .get(validateMessage, messageController.getMessageById)
    .delete(validateMessage, messageController.deleteMessage);