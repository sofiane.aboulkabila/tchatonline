import express from 'express';
import { validateChatRoom } from '../middlewares/validateChatRoom.js';
import * as chatRoomController from '../controllers/chatRoomController.js';

export const chatRoomRouter = express.Router();

chatRoomRouter.route('/chatrooms')
    .get(chatRoomController.getAllChatRooms)
    .post(validateChatRoom, chatRoomController.createChatRoom);

chatRoomRouter.route('/chatrooms/:id')
    .put(validateChatRoom, chatRoomController.updateChatRoom)
    .delete(chatRoomController.deleteChatRoom);