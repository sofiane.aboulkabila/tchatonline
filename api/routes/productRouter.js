import express from 'express';
import { validateProduct } from '../middlewares/validateProduct.js';
import * as productController from '../controllers/productController.js';
import { checkJwtToken } from '../middlewares/checkJwtToken.js';

export const productRouter = express.Router();

productRouter.route('/products')
    .get(validateProduct, productController.getProducts)
    .post(validateProduct, checkJwtToken, productController.createProduct);

productRouter.route('/products/:id')
    .get(validateProduct, checkJwtToken, productController.getProductById)
    .delete(validateProduct, checkJwtToken, productController.deleteProduct);
