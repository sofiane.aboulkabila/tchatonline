import express from 'express';
import { validateUser } from '../middlewares/validateUser.js';
import * as userController from '../controllers/userController.js';
import multer from 'multer';
import path from 'path';

export const userRouter = express.Router();

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        const destinationPath = path.join(process.cwd(), 'img', 'users');
        cb(null, destinationPath);
    },
    filename: function (req, file, cb) {
        const randomName = generateRandomFileName();
        cb(null, randomName + path.extname(file.originalname));
    }
});

const upload = multer({ storage: storage });



userRouter.route('/users')
    .get(validateUser, userController.getUsers)
    .post(validateUser, userController.editUser);

userRouter.route('/users/:id')
    .get(validateUser, userController.getUserById);

userRouter.route('/users/edit/username/:id')
    .put(validateUser, userController.editUsername);

userRouter.route('/users/edit/password/:id')
    .put(validateUser, userController.editPassword);

userRouter.route('/users/edit/color/:id')
    .put(validateUser, userController.editColor);


userRouter.route('/users/edit/image/:id')
    .put(validateUser, upload.single('image'), userController.editImage);

userRouter.route('/users/delete/:id')
    .delete(validateUser, userController.deleteUser);