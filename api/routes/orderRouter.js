import express from 'express';
import * as orderController from '../controllers/orderController.js';
import { validateOrder } from '../middlewares/validateOrder.js';

export const orderRouter = express.Router();

orderRouter.route('/orders')
    .post(validateOrder, orderController.createOrder)
    .get(validateOrder, orderController.getAllOrders);

orderRouter.route('/orders/:id')
    .get(validateOrder, orderController.getOrderById)
    .put(validateOrder, orderController.updateOrder)
    .delete(validateOrder, orderController.deleteOrder);