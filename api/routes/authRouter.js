import express from 'express';
import multer from 'multer';
import { validateAuth } from '../middlewares/validateAuth.js';
import * as authController from '../controllers/authController.js';

const upload = multer({ dest: 'img/user' });

export const authRouter = express.Router();

authRouter.route('/register')
    .post(validateAuth, authController.registerUser);

authRouter.route('/login')
    .post(validateAuth, authController.loginUser);
