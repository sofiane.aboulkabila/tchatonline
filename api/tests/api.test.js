import request from 'supertest';
import app from '../app.js';
import Chance from 'chance';

describe('🛠️   Début des tests unitaires API', () => {
    const chance = new Chance();

    function generateRandomName(minLength, maxLength) {
        return chance.string({ length: chance.integer({ min: minLength, max: maxLength }), pool: 'abcdefghijklmnopqrstuvwxyz' });
    }

    //! AUTH

    let userData = {};

    let username = generateRandomName(3, 20);

    const registerData = {
        username: username,
        email: `${username}@example.com`,
        password: 'test12345',
        role: 'Utilisateur',
        image: 'default.png'
    };

    //* REGISTER
    it('   ✅   Devrait s\'inscrire', async () => {

        const res = await request(app)
            .post('/api/register')
            .send(registerData);

        expect(res.statusCode).toEqual(200);
    });
    //* LOGIN

    it('   🔐   Devrait se connecter', async () => {

        const res = await request(app)
            .post('/api/login')
            .send({
                username: registerData.username,
                password: registerData.password
            });

        expect(res.statusCode).toEqual(200);
        expect(res.body).toBeDefined();

        userData._token = res.body.token;
        userData._id = res.body._id;
    });

    //! USERS

    //* GET ALL USERS

    it('   👤   Devrait récupérer tous les utilisateurs', async () => {

        const res = await request(app)
            .get('/api/users')
            .set('Authorization', userData._token);

        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
    });

    //* GET USER BY ID

    it('   👤   Devrait récupérer un user en particulier', async () => {
        const res = await request(app)
            .get(`/api/user/${userData._id}`)
            .set('Authorization', userData._token);

        expect(res.status).toBe(200);
    });

    //* EDIT USER

    it('   🔄   Devrait modifier un utilisateur existant', async () => {

        const res = await request(app)
            .put(`/api/users/edit/${userData._id}`)
            .set('Authorization', userData._token)
            .send({ email: "nouvelle_email@test.fr" });

        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
    });

    //* DELETE USER

    it('   ❌   Devrait supprimer un utilisateur existant', async () => {

        const res = await request(app)
            .delete(`/api/users/delete/${userData._id}`)
            .set('Authorization', userData._token);

        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
    });

    //! CHAT ROOM

    const chatRoomsName = generateRandomName(3, 20);

    let chatRoomData = {
        name: chatRoomsName,
        users: [],
        messages: []
    };

    //* CREATE CHAT ROOM

    it('   🏠   Devrait créer une salle de chat avec des utilisateurs et des messages', async () => {

        const res = await request(app)
            .post('/api/chatrooms')
            .set('Authorization', userData._token)
            .send(chatRoomData);

        expect(res.statusCode).toEqual(200);

        chatRoomData._id = res.body._id;
    });

    //* GET CHAT ROOM

    it('   🚪   Devrait récupérer une salle de chat avec les utilisateurs et messages', async () => {
        const res = await request(app)
            .get(`/api/chatrooms/${chatRoomData._id}`)
            .set('Authorization', userData._token);

        expect(res.statusCode).toEqual(200);
    });

    //* EDIT CHAT ROOM

    it('   🔄   Devrait modifier une salle de chat', async () => {

        const res = await request(app)
            .put(`/api/chatrooms/${chatRoomData._id}`)
            .set('Authorization', userData._token)
            .send({ name: chatRoomsName, users: [], messages: [] });

        expect(res.statusCode).toEqual(200);
    });

    //* DELETE CHAT ROOM

    it('   ❌   Devrait supprimer une salle de chat', async () => {

        const res = await request(app)
            .delete(`/api/chatrooms/${chatRoomData._id}`)
            .set('Authorization', userData._token);

        expect(res.statusCode).toEqual(200);
    });

    //! MESSAGES

    let messageData = {};

    //* CREATE MESSAGE

    it('   ✉️    Devrait créer un nouveau message', async () => {
        const newMessageData = {
            content: 'Ceci est un nouveau message',
            author: userData._id,
            messageType: 'text'
        };

        const res = await request(app)
            .post('/api/messages')
            .set('Authorization', userData._token)
            .send(newMessageData);

        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();

        messageData._id = res.body._id;
    });


    //* GET ALL MESSAGES

    it('   👤   Devrait récupérer tous les messages', async () => {
        const res = await request(app)
            .get('/api/messages')
            .set('Authorization', userData._token);

        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
    });


    it('   👤   Devrait récupérer un message en particulier', async () => {
        const res = await request(app)
            .get(`/api/messages/${messageData._id}`)
            .set('Authorization', userData._token);

        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
    });

    it('   ❌   Devrait supprimer un message existant', async () => {
        const res = await request(app)
            .delete(`/api/messages/${messageData._id}`)
            .set('Authorization', userData._token);

        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
    });

    //! PRODUCTS

    let productData = {};

    //* CREATE PRODUCT

    const productName = generateRandomName(3, 20);

    it('   ✨   Devrait créer un nouveau produit', async () => {
        const newProductData = {
            name: productName,
            description: 'Description du nouveau produit',
            price: 10,
            quantity: 100,
            category: 'Electronics'
        };

        const res = await request(app)
            .post('/api/products')
            .set('Authorization', userData._token)
            .send(newProductData);

        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();

        productData._id = res.body._id;
    });

    //* GET ALL PRODUCTS

    it('   🛍️    Devrait récupérer tous les produits', async () => {
        const res = await request(app)
            .get('/api/products')
            .set('Authorization', userData._token);

        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
    });

    //* GET PRODUCT BY ID

    it('   🛍️    Devrait récupérer un produit en particulier', async () => {
        const res = await request(app)
            .get(`/api/products/${productData._id}`)
            .set('Authorization', userData._token);

        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
    });

    //* SUPPRIMER PRODUIT

    it('   ❌   Devrait supprimer un produit existant', async () => {
        const res = await request(app)
            .delete(`/api/products/${productData._id}`)
            .set('Authorization', userData._token);

        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
    });

    //! CHARGES STRIPE

    let chargeData = {};

    //* CRÉER UNE CHARGE

    it('   ✨   Devrait créer une nouvelle charge', async () => {
        const newChargeData = {
            amount: 2000,
            source: 'tok_visa',
            currency: 'eur'
        };

        const res = await request(app)
            .post('/api/stripe/charges')
            .set('Authorization', userData._token)
            .send(newChargeData);

        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();

        chargeData._id = res.body.id;
    });

    //* OBTENIR TOUTES LES CHARGES

    it('   🛍️    Devrait récupérer tous les charges', async () => {
        const res = await request(app)
            .get('/api/stripe/charges')
            .set('Authorization', userData._token);

        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
    });

    //* OBTENIR UNE CHARGE PAR ID

    it('   🔍   Devrait récupérer une charge en particulier', async () => {
        const res = await request(app)
            .get(`/api/stripe/charges/${chargeData._id}`)
            .set('Authorization', userData._token);

        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
    });

    //! COMMANDES

    let orderData = {};

    it('   ✨   Devrait créer une nouvelle commande', async () => {
        const newOrderData = {
            product: productData._id,
            user: userData._id,
            status: 'pending',
            deliveryAddress: '123 Street, City, Country'
        };

        const res = await request(app)
            .post('/api/orders')
            .set('Authorization', userData._token)
            .send(newOrderData);

        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();

        orderData._id = res.body._id;
    });

    it('   🛍️    Devrait récupérer toutes les commandes', async () => {
        const res = await request(app)
            .get('/api/orders')
            .set('Authorization', userData._token);

        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
    });

    it('   🛍️    Devrait récupérer une commande spécifique par ID', async () => {
        const res = await request(app)
            .get(`/api/orders/${orderData._id}`)
            .set('Authorization', userData._token);

        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
    });

    it('   🛍️    Devrait mettre à jour une commande', async () => {
        const updatedOrderData = {
            status: 'shipped',
            deliveryAddress: '456 Street, City, Country'
        };

        const res = await request(app)
            .put(`/api/orders/${orderData._id}`)
            .set('Authorization', userData._token)
            .send(updatedOrderData);

        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
    });

    it('   🛍️    Devrait supprimer une commande', async () => {
        const res = await request(app)
            .delete(`/api/orders/${orderData._id}`)
            .set('Authorization', userData._token);

        expect(res.status).toBe(200);
        expect(res.body).toBeDefined();
    });

});
