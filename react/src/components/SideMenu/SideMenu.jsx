import { useState, useEffect } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Sidebar, Menu, MenuItem } from "react-pro-sidebar";
import LogoutRoundedIcon from "@mui/icons-material/LogoutRounded";
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import { useAuth } from '../../hooks/Auth/useAuth'
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import './sideMenu.css'
import PropTypes from 'prop-types';

export const SideMenu = ({ children }) => {
    const menuItems = [
        {
            path: '/admin/utilisateurs ',
            icon: <AccountBoxIcon />,
            label: 'Utilisateurs'
        },
        {
            path: '/admin/messages ',
            icon: <AccountBoxIcon />,
            label: 'Messages'
        }
    ];

    const [collapsed, setCollapsed] = useState(false);
    const location = useLocation();
    const { signOut } = useAuth();
    const navigate = useNavigate();

    useEffect(() => {
        const updateCollapseState = () => setCollapsed(window.innerWidth < 768);
        updateCollapseState();
        window.addEventListener("resize", updateCollapseState);
        return () => window.removeEventListener("resize", updateCollapseState);
    }, []);

    const handleLogout = () => {
        signOut();
        toast.success("Deconnexion réussie !");
        navigate('/');
    };

    return (
        <div style={{ display: "flex", width: "100%" }}>
            <Sidebar collapsed={collapsed} style={{ minWidth: collapsed ? '80px' : '200px', color: 'white' }} className='Sidebar'>
                <Menu menuItemStyles={{
                    button: {
                        '&:hover': {
                            backgroundColor: '#ff7f7f',
                        },
                        '&.ps-active': {
                            backgroundColor: '#0000ff',
                        },
                        '&.ps-active:hover': {
                            backgroundColor: '#0b4edf',
                        },
                    },
                }}>
                    {menuItems.map((item) => (
                        <MenuItem
                            key={item.label}
                            icon={item.icon}
                            active={location.pathname === item.path}
                            component={<Link to={item.path} />}
                        >
                            {item.label}
                        </MenuItem>
                    ))}
                    <MenuItem
                        icon={<LogoutRoundedIcon />}
                        onClick={handleLogout}>
                        Déconnexion
                    </MenuItem>
                </Menu>
            </Sidebar>
            <div style={{ flexGrow: 1, padding: '20px' }}>
                {children}
            </div>
        </div >
    );
};

SideMenu.propTypes = {
    children: PropTypes.node.isRequired,
}
