import { Link } from 'react-router-dom';
import { Container, Typography, Grid, Button } from '@mui/material';

export const Footer = () => {
    const emailAddress = 'contact@example.com';

    const handleContactClick = () => {
        window.location.href = `mailto:${emailAddress}`;
    };

    return (
        <footer style={{ background: '#7B4040', color: 'white', padding: '10px 0', width: '100%' }}>
            <Container>
                <Grid container  >
                    <Grid item xs={12} sm={6} md={4}>
                        <Typography variant="h6" gutterBottom>
                            Informations
                        </Typography>
                        <Typography variant="body2">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sit amet semper nisi.
                        </Typography>
                    </Grid>
                    <Grid item xs={12} sm={6} md={4}>
                        <Typography variant="h6" gutterBottom>
                            Liens utiles
                        </Typography>
                        <ul style={{ color: 'lightblue' }}>
                            <li>
                                <Link to="/mentions-legales" style={{ color: 'lightblue', textDecoration: 'none' }}>Mentions légales</Link>
                            </li>
                            <li>
                                <Link to="/rgpd" style={{ color: 'lightblue', textDecoration: 'none' }}>RGPD</Link>
                            </li>
                        </ul>
                    </Grid>
                    <Grid item xs={12} md={4}>
                        <Typography variant="h6" gutterBottom>
                            Contact
                        </Typography>
                        <Typography variant="body2">
                            Email: {emailAddress}
                        </Typography>
                        <Button variant="contained" onClick={handleContactClick} color="primary" style={{ marginTop: '1rem' }}>
                            Nous contacter
                        </Button>
                    </Grid>
                </Grid>
            </Container>
        </footer>
    );
};
