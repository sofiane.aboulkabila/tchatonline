import { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import {
    AppBar, Box, Toolbar, IconButton, Button, Container, Avatar
} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import { toast } from 'react-toastify';

import { useAuth } from '../../hooks/Auth/useAuth';
import { MenuComponent } from './MenuComponent';

export const Navbar = () => {
    const { isAuthenticated, signOut, auth } = useAuth();
    const navigate = useNavigate();
    const [isMobileView, setIsMobileView] = useState(window.innerWidth <= 768);
    const [menuOpen, setMenuOpen] = useState({ nav: false, user: false });

    let userRole;
    if (isAuthenticated) {
        userRole = auth.role;
    }

    const NavbarConfig = {
        pages: [
            { name: 'Accueil', path: '/' },
            { name: 'Boutique', path: '/boutique' },
            { name: 'Tchat', path: '/tchat' },
            { name: 'Inscription', path: '/inscription', right: true },
            { name: 'Se connecter', path: '/connexion', right: true },
        ],
        settings: [
            { name: 'Profil', path: '/utilisateur/profil' },
            { name: 'Dashboard', path: '/admin/utilisateurs', role: 'Administrateur' },
            { name: 'Se déconnecter' }
        ],
        avatarSrc: "/static/images/avatar/2.jpg"
    };

    useEffect(() => {
        const handleResize = () => {
            setIsMobileView(window.innerWidth <= 768);
        };

        window.addEventListener('resize', handleResize);
        return () => window.removeEventListener('resize', handleResize);
    }, []);


    const toggleMenu = (menu) => setMenuOpen(prev => ({ ...prev, [menu]: !prev[menu] }));
    const filterItemsByRole = (items) => items.filter(item => !item.role || item.role === userRole);


    const handleSignOutAndRedirect = () => {
        signOut();
        toast.success("Déconnexion réussie !");
        navigate('/connexion');
    };

    const getButtonStyle = (pagePath, isMenuItem = false) => {
        const baseStyle = {
            mx: 1,
            fontWeight: '500',
            whiteSpace: 'nowrap',
            color: isMenuItem ? 'black' : 'white',
        };
        return baseStyle;
    };

    return (
        <AppBar position="static" sx={{ bgcolor: '#7b4040' }}  >
            <nav>
                <Container maxWidth="xxl">
                    <Toolbar disableGutters>
                        {isMobileView && (
                            <IconButton onClick={() => toggleMenu('nav')} color="inherit">
                                <MenuIcon sx={{ fontSize: '2rem' }} />
                            </IconButton >
                        )}
                        <MenuComponent
                            anchor="nav"
                            items={filterItemsByRole(NavbarConfig.pages.filter(page => !page.right))}
                            isOpen={menuOpen.nav}
                            toggleMenu={toggleMenu}
                            handleSignOutAndRedirect={handleSignOutAndRedirect}
                            getButtonStyle={getButtonStyle}
                        />
                        {!isMobileView && filterItemsByRole(NavbarConfig.pages.filter(page => !page.right)).map(page => (
                            <Button
                                key={page.name}
                                component={Link}
                                to={page.path}
                                sx={getButtonStyle(page.path)}
                            >
                                {page.name}
                            </Button>
                        ))}
                        <Box sx={{ flexGrow: 1, display: 'flex', justifyContent: 'flex-end', alignItems: 'center' }}>
                            {!isAuthenticated ? (
                                filterItemsByRole(NavbarConfig.pages.filter(page => page.right)).map(page => (
                                    <Button
                                        key={page.name}
                                        component={Link}
                                        to={page.path}
                                        sx={getButtonStyle(page.path)}
                                    >
                                        {page.name}
                                    </Button>
                                ))
                            ) : (
                                <>
                                    <IconButton onClick={() => toggleMenu('user')} sx={{ p: 0 }}>
                                        <Avatar alt="User Avatar" src={NavbarConfig.avatarSrc} />
                                    </IconButton>
                                    <MenuComponent
                                        anchor="user"
                                        items={filterItemsByRole(NavbarConfig.settings)}
                                        isOpen={menuOpen.user}
                                        toggleMenu={toggleMenu}
                                        handleSignOutAndRedirect={handleSignOutAndRedirect}
                                        getButtonStyle={getButtonStyle}
                                        isSetting={true}
                                    />
                                </>
                            )}
                        </Box>
                    </Toolbar>
                </Container>
            </nav>
        </AppBar>
    );
};