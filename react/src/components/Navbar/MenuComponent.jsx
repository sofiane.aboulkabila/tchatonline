import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Menu, MenuItem, Typography } from '@mui/material';

export const MenuComponent = ({ anchor, items, isOpen, toggleMenu, handleSignOutAndRedirect, getButtonStyle, isSetting = false }) => {
    const anchorOrigin = isSetting ? { vertical: 'top', horizontal: 'right', } : { vertical: 'top', horizontal: 'left' };
    const transformOrigin = isSetting ? { vertical: 'top', horizontal: 'right' } : { vertical: 'top', horizontal: 'left' };

    return (
        <Menu
            id={`menu-appbar-${anchor}`}
            anchorOrigin={anchorOrigin}
            keepMounted
            transformOrigin={transformOrigin}
            open={isOpen}
            onClose={() => toggleMenu(anchor)}
            sx={{ mt: '50px' }}
        >
            {items.map((page) => (
                <MenuItem key={page.name} onClick={() => {
                    toggleMenu(anchor);
                    if (page.name === 'Se déconnecter') {
                        handleSignOutAndRedirect();
                    }
                }}>
                    <Typography
                        component={Link}
                        to={page.path}
                        sx={getButtonStyle(page.path, true)}
                    >
                        {page.name}
                    </Typography>
                </MenuItem>
            ))}
        </Menu>
    );
};

MenuComponent.propTypes = {
    anchor: PropTypes.string.isRequired,
    items: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        path: PropTypes.string
    })).isRequired,
    isOpen: PropTypes.bool.isRequired,
    toggleMenu: PropTypes.func.isRequired,
    handleSignOutAndRedirect: PropTypes.func.isRequired,
    getButtonStyle: PropTypes.func.isRequired,
    isSetting: PropTypes.bool
};

