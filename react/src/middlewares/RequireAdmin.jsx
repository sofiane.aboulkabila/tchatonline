import { Navigate, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useAuth } from '../hooks/Auth/useAuth'

export const RequireAdmin = ({ children, fallbackPath }) => {
    const { auth } = useAuth();
    const location = useLocation();
    console.log(auth.role)
    if (auth && auth.role === 'Administrateur') {
        return children;
    } else {
        return <Navigate to={fallbackPath} state={{ from: location }} replace />;
    }
};

RequireAdmin.propTypes = {
    children: PropTypes.node.isRequired,
    fallbackPath: PropTypes.string
};