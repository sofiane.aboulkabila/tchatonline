import { Navigate, useLocation } from 'react-router-dom';
import useIsAuthenticated from 'react-auth-kit/hooks/useIsAuthenticated';
import PropTypes from 'prop-types';

export const NotRequireAuth = ({ children, fallbackPath }) => {
    const isAuthenticated = useIsAuthenticated();
    const location = useLocation();

    if (isAuthenticated) {
        return <Navigate to={fallbackPath} state={{ from: location }} replace />;
    }

    return children;
};

NotRequireAuth.propTypes = {
    children: PropTypes.node.isRequired,
    fallbackPath: PropTypes.string
};