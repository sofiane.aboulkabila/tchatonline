import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import Button from '@mui/joy/Button';
import Typography from '@mui/joy/Typography';
import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import Input from '@mui/joy/Input';

export const UpdateUserForm = (initialValues) => {
    return (
        <Formik
            initialValues={initialValues}

            validationSchema={Yup.object().shape({
                username: Yup.string()
                    .min(3, 'Le nom d’utilisateur doit contenir au moins 3 caractères')
                    .max(20, 'Le nom d’utilisateur ne peut pas dépasser 20 caractères')
                    .required('Le nom d’utilisateur est requis.'),
                email: Yup.string()
                    .email('Email invalide')
                    .min(5, 'L’adresse email doit contenir au moins 5 caractères')
                    .max(50, 'L’adresse email ne peut pas dépasser 50 caractères')
                    .required('Email est requis'),
                role: Yup.string().required('Le rôle est requis'),
                color: Yup.string().required('La couleur est requise')
            })}

            onSubmit={(values, { setSubmitting }) => {
                console.log(values);
                setSubmitting(false);
            }}
        >
            {({ isSubmitting }) => (
                <Form style={{ textAlign: 'center' }}>
                    <Typography
                        sx={{
                            textAlign: 'center',
                            padding: '15px 10px',
                            backgroundColor: 'blue',
                            fontSize: '1.2em',
                            color: 'white',
                            borderRadius: '10px',
                            marginBottom: '20px'
                        }}>
                        Inscription
                    </Typography>

                    <FormControl sx={{ marginBottom: '15px' }}>
                        <FormLabel>Nom d’utilisateur</FormLabel>
                        <Field as={Input} name="username" type="text" placeholder="Votre nom d’utilisateur" />
                        <ErrorMessage name="username" component="div" className="errorInput" />
                    </FormControl>

                    <FormControl sx={{ marginBottom: '15px' }}>
                        <FormLabel>Email</FormLabel>
                        <Field as={Input} name="email" type="email" placeholder="Votre email" />
                        <ErrorMessage name="email" component="div" className="errorInput" />
                    </FormControl>

                    <FormControl sx={{ marginBottom: '15px' }}>
                        <FormLabel>Rôle</FormLabel>
                        <Field as={Input} name="role" type="text" placeholder="Votre rôle" />
                        <ErrorMessage name="role" component="div" className="errorInput" />
                    </FormControl>

                    <FormControl sx={{ marginBottom: '15px' }}>
                        <FormLabel>Couleur</FormLabel>
                        <Field as={Input} name="color" type="color" placeholder="Choisir une couleur" />
                        <ErrorMessage name="color" component="div" className="errorInput" />
                    </FormControl>

                    <Button type="submit" disabled={isSubmitting} sx={{ mt: 2 }}>
                        S'inscrire
                    </Button>
                </Form>
            )}
        </Formik>
    );
};
