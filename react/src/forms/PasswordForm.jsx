import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import Button from '@mui/joy/Button';
import Typography from '@mui/joy/Typography';
import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import Input from '@mui/joy/Input';

import { useUpdatePassword } from '../hooks/Api/useUpdatePassword';

export const PasswordForm = () => {
    const updatePassword = useUpdatePassword();

    return (
        <Formik
            initialValues={{
                old_password: '',
                new_password: '',
            }}
            validationSchema={Yup.object().shape({
                old_password: Yup.string()
                    .min(8, 'Le mot de passe doit contenir au moins 8 caractères')
                    .max(100, 'Le mot de passe ne peut pas dépasser 100 caractères')
                    .required('Un mot de passe est nécessaire'),
                new_password: Yup.string()
                    .min(8, 'Le mot de passe doit contenir au moins 8 caractères')
                    .max(100, 'Le mot de passe ne peut pas dépasser 100 caractères')
                    .required('Un mot de passe est nécessaire'),
            })}
            onSubmit={(values, { setSubmitting }) => {

                updatePassword(values, setSubmitting);

            }}
        >
            {({ isSubmitting }) => (
                <Form style={{ textAlign: 'center', padding: '20px' }}>
                    <Typography
                        sx={{
                            textAlign: 'center',
                            padding: '15px 10px',
                            backgroundColor: 'blue',
                            fontSize: '1.2em',
                            color: 'white',
                            borderRadius: '10px',
                            marginBottom: '20px'
                        }}>
                        Modifier le mot de passe
                    </Typography>

                    <FormControl sx={{ marginBottom: '15px' }}>
                        <FormLabel>Ancien mot de passe</FormLabel>
                        <Field as={Input} name="old_password" type="password" placeholder="Votre mot de passe" />
                        <ErrorMessage name="old_password" component="div" className="errorInput" />
                    </FormControl>

                    <FormControl sx={{ marginBottom: '15px' }}>
                        <FormLabel>Nouveau mot de passe</FormLabel>
                        <Field as={Input} name="new_password" type="password" placeholder="Votre mot de passe" />
                        <ErrorMessage name="new_password" component="div" className="errorInput" />
                    </FormControl>

                    <Button type="submit" disabled={isSubmitting} sx={{ mt: 2 }}>
                        Confirmer les changements
                    </Button>
                </Form>
            )
            }
        </Formik >
    );
};
