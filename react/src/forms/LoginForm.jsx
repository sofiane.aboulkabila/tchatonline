import { useEffect, useState } from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import Button from '@mui/joy/Button';
import Typography from '@mui/joy/Typography';
import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import Input from '@mui/joy/Input';
import Checkbox from '@mui/joy/Checkbox';
import secureLocalStorage from 'react-secure-storage';
import { useLogin } from '../hooks/Api/useLogin';

export const LoginForm = () => {
    const [initialValues, setInitialValues] = useState({
        username: '',
        password: '',
        rememberMe: false
    });

    const login = useLogin();

    useEffect(() => {
        const loadCredentials = async () => {
            const rememberMe = secureLocalStorage.getItem('rememberMe') === 'true';
            if (rememberMe) {
                const username = secureLocalStorage.getItem('username') || '';
                const password = secureLocalStorage.getItem('password') || '';
                setInitialValues({
                    username,
                    password,
                    rememberMe
                });
            }
        };
        loadCredentials();
    }, []);

    return (
        <Formik
            initialValues={initialValues}
            enableReinitialize
            validationSchema={Yup.object({
                username: Yup.string()
                    .min(3, 'Le nom d’utilisateur doit contenir au moins 3 caractères')
                    .max(20, 'Le nom d’utilisateur doit contenir moins de 20 caractères.')
                    .required('Le nom d’utilisateur est requis.'),
                password: Yup.string()
                    .min(8, 'Le mot de passe doit contenir au moins 8 caractères')
                    .max(100, 'Le mot de passe doit contenir moins de 100 caractères.')
                    .required('Un mot de passe valide est nécessaire.'),
            })}
            onSubmit={async (values, { setSubmitting }) => {

                login(values, setSubmitting);

            }}
        >
            {({ isSubmitting, values, handleChange }) => (
                <Form style={{ textAlign: 'center', padding: '20px' }}>
                    <Typography
                        sx={{
                            textAlign: 'center',
                            padding: '15px 10px',
                            backgroundColor: 'blue',
                            fontSize: '1.2em',
                            color: 'white',
                            borderRadius: '10px',
                            marginBottom: '20px'
                        }}>
                        Se connecter
                    </Typography>

                    <FormControl sx={{ marginBottom: '15px' }}>
                        <FormLabel>Nom d’utilisateur</FormLabel>
                        <Field as={Input} name="username" type="text" placeholder="Votre nom d’utilisateur" />
                        <ErrorMessage name="username" component="div" className="errorInput" />
                    </FormControl>

                    <FormControl sx={{ marginBottom: '15px' }}>
                        <FormLabel>Mot de passe</FormLabel>
                        <Field as={Input} name="password" type="password" placeholder="Votre mot de passe" />
                        <ErrorMessage name="password" component="div" className="errorInput" />
                    </FormControl>

                    <FormControl sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Checkbox
                            name="rememberMe"
                            checked={values.rememberMe}
                            onChange={handleChange}
                            sx={{ '& .MuiSvgIcon-root': { fontSize: 28 } }}
                        />
                        <Typography sx={{ marginLeft: '10px' }}>Se souvenir de moi</Typography>
                    </FormControl>

                    <Button type="submit" disabled={isSubmitting} sx={{ mt: 2, backgroundColor: 'blue' }}>
                        Se connecter
                    </Button>
                </Form>
            )}
        </Formik>
    );
};
