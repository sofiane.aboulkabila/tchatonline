import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import Button from '@mui/joy/Button';
import Typography from '@mui/joy/Typography';
import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import Input from '@mui/joy/Input';

import { useUpdateUsername } from '../hooks/Api/useUpdateUsername';

export const PseudoForm = () => {
    const updatePseudo = useUpdateUsername();

    return (
        <Formik
            initialValues={{
                username: '',
                password: ''
            }}
            validationSchema={Yup.object().shape({
                username: Yup.string()
                    .min(3, 'Le pseudo doit contenir au moins 3 caractères')
                    .max(20, 'Le pseudo ne peut pas dépasser 20 caractères')
                    .required('Le nouveau pseudo est requis.'),
                password: Yup.string()
                    .min(8, 'Le mot de passe doit contenir au moins 8 caractères')
                    .max(100, 'Le mot de passe ne peut pas dépasser 100 caractères')
                    .required('Un mot de passe est sélectionné')
            })}
            onSubmit={(values, { setSubmitting }) => {

                updatePseudo(values, setSubmitting);

            }}
        >
            {({ isSubmitting }) => (
                <Form style={{ textAlign: 'center', padding: '20px' }}>
                    <Typography
                        sx={{
                            textAlign: 'center',
                            padding: '15px 10px',
                            backgroundColor: 'blue',
                            fontSize: '1.2em',
                            color: 'white',
                            borderRadius: '10px',
                            marginBottom: '20px'
                        }}>
                        Modifier le Pseudo
                    </Typography>

                    <FormControl sx={{ marginBottom: '15px' }}>
                        <FormLabel>Nouveau pseudo</FormLabel>
                        <Field as={Input} name="username" type="text" placeholder="Entrez votre nouveau pseudo" />
                        <ErrorMessage name="username" component="div" className="errorInput" />
                    </FormControl>


                    <FormControl sx={{ marginBottom: '15px' }}>
                        <FormLabel>Mot de passe</FormLabel>
                        <Field as={Input} name="password" type="password" placeholder="Votre mot de passe" />
                        <ErrorMessage name="password" component="div" className="errorInput" />
                    </FormControl>


                    <Button type="submit" disabled={isSubmitting} sx={{ mt: 2 }}>
                        Confirmer les changements
                    </Button>
                </Form>
            )}
        </Formik>
    );
};
