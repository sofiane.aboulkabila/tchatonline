import { useRegister } from '../hooks/Api/useRegister';
import * as Yup from 'yup';
import Button from '@mui/joy/Button';
import Typography from '@mui/joy/Typography';
import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import Input from '@mui/joy/Input';
import { Formik, Field, Form, ErrorMessage } from 'formik';

export const RegisterForm = () => {
    const register = useRegister();

    return (
        <Formik
            initialValues={{
                username: '',
                email: '',
                password: '',
                confirmPassword: '',
                image: 'default.png',
                role: 'Utilisateur'
            }}

            validationSchema={Yup.object().shape({
                username: Yup.string()
                    .min(3, 'Le nom d’utilisateur doit contenir au moins 3 caractères')
                    .max(20, 'Le nom d’utilisateur ne peut pas dépasser 20 caractères')
                    .required('Le nom d’utilisateur est requis.'),
                email: Yup.string()
                    .email('Email invalide')
                    .min(5, 'L’adresse email doit contenir au moins 5 caractères')
                    .max(50, 'L’adresse email ne peut pas dépasser 50 caractères')
                    .required('Email est requis'),
                password: Yup.string()
                    .min(8, 'Le mot de passe doit contenir au moins 8 caractères')
                    .max(100, 'Le mot de passe ne peut pas dépasser 100 caractères')
                    .required('Un mot de passe est nécessaire'),
                confirmPassword: Yup.string()
                    .oneOf([Yup.ref('password'), null], 'Les mots de passe doivent correspondre')
                    .required('La confirmation du mot de passe est requise'),
                image: Yup.string()
            })}

            onSubmit={async (values, { setSubmitting }) => {
                register(values, setSubmitting);
            }}
        >
            {({ isSubmitting }) => (
                <Form style={{ textAlign: 'center', padding: '20px' }}>
                    <Typography
                        sx={{
                            textAlign: 'center',
                            padding: '15px 10px',
                            backgroundColor: 'blue',
                            fontSize: '1.2em',
                            color: 'white',
                            borderRadius: '10px',
                            marginBottom: '20px'
                        }}>
                        Inscription
                    </Typography>

                    <FormControl sx={{ marginBottom: '15px' }}>
                        <FormLabel>Nom d’utilisateur</FormLabel>
                        <Field as={Input} name="username" type="text" placeholder="Votre nom d’utilisateur" />
                        <ErrorMessage name="username" component="div" className="errorInput" />
                    </FormControl>

                    <FormControl sx={{ marginBottom: '15px' }}>
                        <FormLabel>Mot de passe</FormLabel>
                        <Field as={Input} name="password" type="password" placeholder="Votre mot de passe" />
                        <ErrorMessage name="password" component="div" className="errorInput" />
                    </FormControl>

                    <FormControl sx={{ marginBottom: '15px' }}>
                        <FormLabel>Confirmer le mot de passe</FormLabel>
                        <Field as={Input} name="confirmPassword" type="password" placeholder="Confirmez votre mot de passe" />
                        <ErrorMessage name="confirmPassword" component="div" className="errorInput" />
                    </FormControl>

                    <FormControl >
                        <FormLabel>Email</FormLabel>
                        <Field as={Input} name="email" type="email" placeholder="Votre email" />
                        <ErrorMessage name="email" component="div" className="errorInput" />
                    </FormControl>

                    <Button type="submit" disabled={isSubmitting} sx={{ mt: 2 }}>
                        S'inscrire
                    </Button>
                </Form>
            )}
        </Formik>
    );
};
