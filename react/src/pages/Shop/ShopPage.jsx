import { useState, useEffect } from 'react';
import axios from 'axios';
import { useAuth } from '../../hooks/Auth/useAuth';
import { Card, CardContent, Typography, Grid, Pagination, Stack, Select, MenuItem, FormControl, Button, CardActions, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@mui/material';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

export const ShopPage = () => {
    const [products, setProducts] = useState([]);
    const [filteredProducts, setFilteredProducts] = useState([]);
    const [categories, setCategories] = useState([]);
    const [selectedCategory, setSelectedCategory] = useState('');
    const [currentPage, setCurrentPage] = useState(1);
    const [productsPerPage] = useState(10);
    const { isAuthenticated, authHeader, auth } = useAuth();
    const navigate = useNavigate();
    const [loginPromptOpen, setLoginPromptOpen] = useState(false);

    const theme = createTheme({
        palette: {
            primary: { main: '#7B4040' },
            secondary: { main: '#FFFFFF' }
        }
    });

    useEffect(() => {
        const fetchProducts = async () => {
            try {
                const response = await axios.get('http://localhost:8000/api/products');
                setProducts(response.data);
                setFilteredProducts(response.data);
                setCategories([...new Set(response.data.map(p => p.category))]);
            } catch (error) {
                console.error('Error fetching products:', error);
            }
        };
        fetchProducts();
    }, [authHeader]);

    useEffect(() => {
        setFilteredProducts(selectedCategory ? products.filter(p => p.category === selectedCategory) : products);
        setCurrentPage(1);
    }, [selectedCategory, products]);

    const handleChangePage = (event, newPage) => setCurrentPage(newPage);

    const handleBuy = async (product) => {
        if (!isAuthenticated) {
            setLoginPromptOpen(true);
            return;
        }
        try {
            const response = await axios.post(`http://localhost:8000/api/stripe/charges/${auth._id}`, {
                name: product.name,
                amount: Math.round(product.price * 100),
                source: 'tok_visa',
                currency: 'eur',
                description: product.description
            }, { headers: { Authorization: authHeader } });
            if (response.data.success) {
                toast.success(`Paiement réussi, url de facturation: ${response.data.receipt_url}`);
            } else {
                toast.error('Paiement échoué');
            }
        } catch (error) {
            toast.error('Une erreur est survenue lors du paiement');
        }
    };

    const closeLoginPrompt = () => setLoginPromptOpen(false);
    const redirectToLogin = () => navigate('/connexion');

    const indexOfLastProduct = currentPage * productsPerPage;
    const indexOfFirstProduct = indexOfLastProduct - productsPerPage;
    const currentProducts = filteredProducts.slice(indexOfFirstProduct, indexOfLastProduct);

    return (
        <ThemeProvider theme={theme}>
            <div style={{ display: 'flex', alignItems: 'center', height: '92vh', flexDirection: 'column', margin: 'auto', padding: '15px 20px' }}>
                <FormControl>
                    <Select value={selectedCategory} label="Category" onChange={e => setSelectedCategory(e.target.value)} displayEmpty style={{ marginBottom: '20px', width: '200px', background: 'white', border: '1px solid black' }}>
                        <MenuItem value="">Tout voir</MenuItem>
                        {categories.map(category => <MenuItem key={category} value={category}>{category}</MenuItem>)}
                    </Select>
                </FormControl>
                <Grid container spacing={1} justifyContent="center">
                    {currentProducts.map(product => (
                        <Grid item key={product._id} xs={12} sm={6} md={4} lg={3}>
                            <Card raised sx={{ bgcolor: 'background.paper' }}>
                                <CardContent>
                                    <Typography gutterBottom variant="h5">{product.name}</Typography>
                                    <Typography variant="body2" color="text.secondary">{product.description}</Typography>
                                    <Typography variant="body1" style={{ marginTop: '5px' }}>€{product.price.toFixed(2)}</Typography>
                                    <Typography variant="body2" color="text.secondary" style={{ fontSize: '0.875rem' }}>{product.quantity} en stock • {product.category}</Typography>
                                </CardContent>
                                <CardActions>
                                    <Button
                                        fullWidth
                                        variant="contained"
                                        color="primary"
                                        onClick={() => handleBuy(product)}
                                        disabled={isAuthenticated && auth.color && product.name === 'color' ? true : false}
                                    >
                                        Acheter pour €{product.price.toFixed(2)}
                                    </Button>
                                </CardActions>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
                <Stack spacing={2} justifyContent="center" alignItems="center" mt={2}>
                    <Pagination count={Math.ceil(filteredProducts.length / productsPerPage)} page={currentPage} onChange={handleChangePage} color="primary" showFirstButton showLastButton />
                </Stack>
                <Dialog open={loginPromptOpen} onClose={closeLoginPrompt}>
                    <DialogTitle>Il faut être connecté.</DialogTitle>
                    <DialogContent>
                        <DialogContentText>Pour procéder à votre achat, merci de bien vouloir vous connecter.</DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button variant='contained' onClick={closeLoginPrompt} color="primary">Annuler</Button>
                        <Button variant='contained' onClick={redirectToLogin} color="success">Se connecter</Button>
                    </DialogActions>
                </Dialog>
            </div>
        </ThemeProvider>
    );
};
