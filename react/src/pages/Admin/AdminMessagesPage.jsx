import { useState, useEffect, useCallback } from 'react';
import { Box, IconButton, Typography, Dialog, DialogActions, DialogContent, DialogTitle, Button } from '@mui/material';
import axios from 'axios';
import { MaterialReactTable } from 'material-react-table';
import { MRT_Localization_FR } from 'material-react-table/locales/fr';
import { useAuth } from "../../hooks/Auth/useAuth";
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import { SideMenu } from "../../components/SideMenu/SideMenu";
import PropTypes from 'prop-types';

export const AdminMessagesPage = () => {
    const [data, setData] = useState([]);
    const [editModalOpen, setEditModalOpen] = useState(false);
    const [deleteModalOpen, setDeleteModalOpen] = useState(false);
    const [currentMessageId, setCurrentMessageId] = useState(null);
    const [initialValues, setInitialValues] = useState({});
    const { authHeader } = useAuth();

    const fetchData = useCallback(async () => {
        try {
            const response = await axios.get('http://localhost:8000/api/messages', { headers: { Authorization: authHeader } });
            const messagesWithData = await Promise.all(response.data.map(async (message) => {
                const authorResponse = await axios.get(`http://localhost:8000/api/users/${message.author}`, { headers: { Authorization: authHeader } });
                const authorUsername = authorResponse.data.username;
                return { ...message, authorUsername };
            }));
            setData(messagesWithData);
        } catch (error) {
            console.error('Error fetching messages:', error);
        }
    }, [authHeader]);

    useEffect(() => {
        fetchData();
    }, [fetchData]);

    const handleOpenEditModal = (messageData) => {
        setInitialValues(messageData);
        setEditModalOpen(true);
    };

    const handleOpenDeleteModal = (messageId) => {
        setCurrentMessageId(messageId);
        setDeleteModalOpen(true);
    };

    const handleCloseModals = () => {
        setEditModalOpen(false);
        setDeleteModalOpen(false);
    };

    const handleDeleteMessage = async () => {
        try {
            await axios.delete(`http://localhost:8000/api/messages/${currentMessageId}`, { headers: { Authorization: authHeader } });
            setData(prevData => prevData.filter(message => message._id !== currentMessageId));
            handleCloseModals();
        } catch (error) {
            console.error('Error deleting message:', error);
        }
    };

    const columns = [
        { accessorKey: 'content', header: 'Contenu' },
        { accessorKey: 'authorUsername', header: 'Auteur' },
        { accessorKey: 'createdAt', header: 'Date de création' },
        {
            accessorKey: '_id', id: 'actions', header: 'Actions',
            Cell: ({ row }) => (
                <Box sx={{ display: 'flex', gap: 1 }}>
                    <IconButton color="primary" onClick={() => handleOpenEditModal(row.original)}>
                        <EditIcon />
                    </IconButton>
                    <IconButton color="error" onClick={() => handleOpenDeleteModal(row.original._id)}>
                        <DeleteIcon />
                    </IconButton>
                </Box>
            ),
            propTypes: {
                row: PropTypes.shape({
                    original: PropTypes.object.isRequired
                }).isRequired
            }
        }
    ];

    return (
        <SideMenu>
            <Box sx={{ display: 'flex', justifyContent: 'flex-end', mb: 3 }}>
                <Button variant="contained" color='success' onClick={() => handleOpenEditModal({})}>Ajouter un message</Button>
            </Box>
            <MaterialReactTable
                columns={columns}
                data={data}
                getRowId={row => row._id}
                localization={MRT_Localization_FR}
            />
            <Dialog open={editModalOpen} onClose={handleCloseModals}>
                <DialogContent>
                    <Typography>Modification du message</Typography>
                </DialogContent>
            </Dialog>
            <Dialog open={deleteModalOpen} onClose={handleCloseModals}>
                <DialogTitle>Supprimer Message?</DialogTitle>
                <DialogContent>
                    <Typography>Êtes-vous sûr de vouloir supprimer ce message?</Typography>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleDeleteMessage}>Confirmer</Button>
                    <Button onClick={handleCloseModals}>Annuler</Button>
                </DialogActions>
            </Dialog>
        </SideMenu>
    );
};
