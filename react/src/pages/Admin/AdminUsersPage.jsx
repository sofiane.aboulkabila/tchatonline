import { useState, useEffect, useCallback } from 'react';
import { Box, Button, IconButton, Typography, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import axios from 'axios';
import { MaterialReactTable } from 'material-react-table';
import { MRT_Localization_FR } from 'material-react-table/locales/fr';
import { useAuth } from "../../hooks/Auth/useAuth";
import { UpdateUserForm } from "../../forms/UpdateUserForm";
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import { SideMenu } from "../../components/SideMenu/SideMenu";
import PropTypes from 'prop-types';

export const AdminUsersPage = () => {
    const [data, setData] = useState([]);
    const [editModalOpen, setEditModalOpen] = useState(false);
    const [deleteModalOpen, setDeleteModalOpen] = useState(false);
    const [currentUserId, setCurrentUserId] = useState(null);
    const [initialValues, setInitialValues] = useState({});
    const { authHeader } = useAuth();

    const fetchData = useCallback(async () => {
        try {
            const response = await axios.get('http://localhost:8000/api/users', { headers: { Authorization: authHeader } });
            setData(response.data);
        } catch (error) {
            console.error('Error fetching users:', error);
        }
    }, [authHeader]);

    useEffect(() => {
        fetchData();
    }, [fetchData]);

    const handleOpenEditModal = (userData) => {
        setInitialValues(userData);
        setEditModalOpen(true);
    };

    const handleOpenDeleteModal = (userId) => {
        setCurrentUserId(userId);
        setDeleteModalOpen(true);
    };

    const handleCloseModals = () => {
        setEditModalOpen(false);
        setDeleteModalOpen(false);
    };

    const handleDeleteUser = async () => {
        try {
            await axios.delete(`http://localhost:8000/api/user/${currentUserId}`, { headers: { Authorization: authHeader } });
            setData(prevData => prevData.filter(user => user._id !== currentUserId));
            handleCloseModals();
        } catch (error) {
            console.error('Error deleting user:', error);
        }
    };

    const handleUpdateUser = async (updatedUserData) => {
        try {
            console.log('Updated user data:', updatedUserData);
            setEditModalOpen(false);
        } catch (error) {
            console.error('Error updating user:', error);
        }
    };

    const columns = [
        { accessorKey: 'username', header: 'Nom d\'utilisateur' },
        { accessorKey: 'email', header: 'Email' },
        { accessorKey: 'role', header: 'Rôle' },
        { accessorKey: 'color', header: 'Couleur username' },
        {
            accessorKey: '_id', id: 'actions', header: 'Actions',
            Cell: ({ row }) => (
                <Box sx={{ display: 'flex', gap: 1 }}>
                    <IconButton color="primary" onClick={() => handleOpenEditModal(row.original)}>
                        <EditIcon />
                    </IconButton>
                    <IconButton color="error" onClick={() => handleOpenDeleteModal(row.original._id)}>
                        <DeleteIcon />
                    </IconButton>
                </Box>
            ),
            propTypes: {
                row: PropTypes.shape({
                    original: PropTypes.object.isRequired
                }).isRequired
            }
        }
    ];

    return (
        <SideMenu>
            <Box sx={{ display: 'flex', justifyContent: 'flex-end', mb: 3 }}>
                <Button variant="contained" color='success' onClick={() => handleOpenEditModal({})}>Ajouter un utilisateur</Button>
            </Box>
            <MaterialReactTable
                columns={columns}
                data={data}
                getRowId={row => row._id}
                localization={MRT_Localization_FR}
            />
            <Dialog open={editModalOpen} onClose={handleCloseModals}>
                <DialogContent>
                    <UpdateUserForm initialValues={initialValues} onSubmit={handleUpdateUser} />
                </DialogContent>
            </Dialog>

            <Dialog open={deleteModalOpen} onClose={handleCloseModals}>
                <DialogTitle>Supprimer Utilisateur?</DialogTitle>
                <DialogContent>
                    <Typography>Êtes-vous sûr de vouloir supprimer cet utilisateur?</Typography>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleDeleteUser}>Confirmer</Button>
                    <Button onClick={handleCloseModals}>Annuler</Button>
                </DialogActions>
            </Dialog>
        </SideMenu>
    );
};
