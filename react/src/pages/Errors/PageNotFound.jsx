import { Link } from 'react-router-dom';
import { Box, Typography, Button } from '@mui/material';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

export const PageNotFound = () => {
    const navigate = useNavigate();

    useEffect(() => {
        navigate('/erreur_404', { replace: true });
    }, [navigate]);

    return (
        <Box
            sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: '92vh',

            }}
        >
            <Box
                sx={{
                    textAlign: 'center',
                    padding: '2rem',
                    backgroundColor: '#f0f0f0',
                    borderRadius: '8px',
                    border: '2px solid #ccc',
                    boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)',
                }}
            >

                <Typography variant="h4" gutterBottom>
                    Page introuvable
                </Typography>

                <Typography variant="body1" gutterBottom>
                    Désolé, la page que vous recherchez est introuvable.
                </Typography>

                <Link to="/">
                    <Button
                        sx={{
                            marginTop: '1rem',
                            textTransform: 'none',
                            backgroundColor: '#419021',
                            fontWeight: '600',
                            '&:hover': {
                                backgroundColor: 'green',
                            }
                        }}
                        variant="contained"
                    >
                        Retourner à l'accueil
                    </Button>
                </Link>

            </Box>
        </Box>
    );
};
