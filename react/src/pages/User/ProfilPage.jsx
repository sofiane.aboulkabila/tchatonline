import { useState } from 'react';
import { Typography, Button, Grid, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@mui/material';
import LockIcon from '@mui/icons-material/Lock';
import LockOpenIcon from '@mui/icons-material/LockOpen';
import { PseudoForm } from '../../forms/PseudoForm';
import { PasswordForm } from '../../forms/PasswordForm'
import { ColorForm } from '../../forms/ColorForm'
import { useAuth } from '../../hooks/Auth/useAuth';
import PROFIL_DEFAULT from '../../img/PROFIL_DEFAULT.png';
import { Link } from 'react-router-dom';

export const ProfilPage = () => {
    const [openPseudo, setOpenPseudo] = useState(false);
    const [openPassword, setOpenPassword] = useState(false);
    const [openImage, setOpenImage] = useState(false);
    const [openColor, setOpenColor] = useState(false);
    const [openNotBuy, setOpenNotBuy] = useState(false);
    const { auth } = useAuth();

    const handleClickOpen = (type) => {
        if (type === 'pseudo') {
            setOpenPseudo(true);
        } else if (type === 'password') {
            setOpenPassword(true);
        } else if (type === 'color') {
            setOpenColor(true);
        } else if (type === 'image') {
            setOpenImage(true);
        } else if (type == 'notBuy') {
            setOpenNotBuy(true);
        }
    };

    const handleClose = (type) => {
        if (type === 'pseudo') {
            setOpenPseudo(false);
        } else if (type === 'password') {
            setOpenPassword(false);
        } else if (type === 'color') {
            setOpenColor(false);
        } else if (type === 'image') {
            setOpenImage(false);
        } else if (type === 'notBuy') {
            setOpenNotBuy(false);
        }
    };

    return (
        <div style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '92vh'
        }}>
            <div style={{
                backgroundColor: 'white',
                borderRadius: '20px',
                padding: '20px 50px 20px 20px',
                margin: '0 30px',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center'
            }}>
                <Grid container spacing={2} alignItems="center" justifyContent="center">
                    <Grid item xs={12} md={6} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                        <img src={PROFIL_DEFAULT} alt="Profile" style={{
                            backgroundSize: 'cover',
                            height: '250px',
                            width: '250px',
                            borderRadius: '50%'
                        }} />
                        <Typography variant="h2" style={{ marginLeft: '10px', color: auth.color, margin: '0 30px' }}>{auth.username}</Typography>
                    </Grid>
                    <Grid item xs={12} md={6} style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                        <Button variant="contained" color="primary" sx={{ width: 'fit-content', margin: '10px' }} onClick={() => handleClickOpen('pseudo')}>
                            Modifier le pseudo
                        </Button>
                        <Button variant="contained" sx={{ width: 'fit-content', margin: '10px' }} onClick={() => handleClickOpen('password')}>
                            Modifier le mot de passe
                        </Button>
                        <Button variant="contained" sx={{ width: 'fit-content', margin: '10px' }} onClick={() => handleClickOpen('image')}>
                            Modifier l'image de profil
                        </Button>
                        {auth.color ? (
                            <Button
                                variant="contained"
                                endIcon={<LockOpenIcon />}
                                color="success"
                                sx={{ width: 'fit-content', margin: '10px', whiteSpace: 'nowrap' }}
                                onClick={() => handleClickOpen('color')}
                            >
                                Modifier la couleur du pseudo
                            </Button>
                        ) : (
                            <Button
                                variant="contained"
                                endIcon={<LockIcon />}
                                color="error"
                                sx={{ width: 'fit-content', margin: '10px', whiteSpace: 'nowrap' }}
                                onClick={() => handleClickOpen('notBuy')}
                            >
                                Modifier la couleur du pseudo
                            </Button>
                        )}
                    </Grid>
                </Grid>
            </div>

            <Dialog open={openPseudo} onClose={() => handleClose('pseudo')} aria-labelledby="form-dialog-title">
                <PseudoForm />
            </Dialog>

            <Dialog open={openPassword} onClose={() => handleClose('password')} aria-labelledby="form-dialog-title">
                <PasswordForm />
            </Dialog>

            <Dialog open={openColor} onClose={() => handleClose('color')} aria-labelledby="form-dialog-title">
                <ColorForm color={auth.color} />
            </Dialog>

            <Dialog open={openNotBuy} onClose={() => handleClose('notBuy')} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Ce contenu n'est pas disponible</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Cet article est payant. Veuillez vous rendre dans la boutique pour effectuer un achat.
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => handleClose('notBuy')} color="error" variant="contained">
                        Fermer
                    </Button>
                    <Link to="/boutique" style={{ textDecoration: 'none' }}>
                        <Button color="primary" variant="contained">
                            Aller vers la boutique
                        </Button>
                    </Link>
                </DialogActions>
            </Dialog>

            <Dialog open={openImage} onClose={() => handleClose('image')} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Modifier l'image</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        image
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => handleClose('image')} color="primary">
                        Fermer
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
};
