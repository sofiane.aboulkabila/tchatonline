import { LoginForm } from '../../forms/LoginForm';
import Sheet from '@mui/joy/Sheet';

export const LoginPage = () => {
    return (
        <main>
            <div className="LoginPage" style={{
                height: '92vh',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <Sheet
                    sx={{
                        width: 350,
                        background: 'white',
                        margin: 'auto',
                        display: 'flex',
                        flexDirection: 'column',
                        gap: 2,
                        borderRadius: 'md',
                        boxShadow: 'md',
                    }}
                    variant="outlined"
                >

                    <LoginForm />
                </Sheet>
            </div>
        </main>
    );
};
