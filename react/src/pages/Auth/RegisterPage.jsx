import { RegisterForm } from '../../forms/RegisterForm';
import Sheet from '@mui/joy/Sheet';

export const RegisterPage = () => {
    return (
        <main>
            <div className="LoginPage" style={{
                height: '88vh',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <Sheet
                    sx={{
                        width: '350px',
                        background: 'white',
                        margin: 'auto',
                        display: 'flex',
                        flexDirection: 'column',
                        gap: 2,
                        borderRadius: 'md',
                        boxShadow: 'md',
                    }}
                    variant="outlined"
                >

                    <RegisterForm />
                </Sheet>
            </div>
        </main>
    );
};
