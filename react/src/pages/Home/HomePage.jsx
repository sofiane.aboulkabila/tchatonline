import { Link } from 'react-router-dom';
import { Typography, Button, Box } from '@mui/material';
import { useAuth } from '../../hooks/Auth/useAuth';
import HOME_BACKGROUND from '../../img/HOME_BACKGROUND.png'
import { Footer } from '../../components/Footer/Footer';

export const HomePage = () => {
    const { auth, isAuthenticated } = useAuth();

    return (
        <>
            <Box sx={{
                color: 'red',
                fontSize: '50px',
                backgroundImage: 'url( ' + HOME_BACKGROUND + ')',
                backgroundSize: 'cover',
                height: 'calc(77vh + 6px)',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                textAlign: 'center',
                animation: 'fadeIn 2s ease-out'
            }}>
                <Box sx={{
                    backgroundColor: 'rgba(0, 0, 0, 0.5)',
                    padding: '20px',
                    borderRadius: '10px',
                    color: 'white'
                }}>
                    {isAuthenticated ? (
                        <>
                            <Typography variant="h2" component="h1">Bonjour {auth.username} !</Typography>
                            <Typography variant="h6" sx={{ mt: 2 }}>Prêt à discuter avec d'autres utilisateurs ?</Typography>
                            <Button component={Link} to="/tchat" variant="contained" color="primary" sx={{ mt: 2, fontSize: '20px', fontWeight: 'bold', boxShadow: 2 }}>
                                Accéder au Tchat
                            </Button>
                        </>
                    ) : (
                        <>
                            <Typography variant="h2" component="h1">Bienvenue sur TchatOnline 3WA ! </Typography>
                            <Typography variant="h6" sx={{ mt: 2 }}>Connectez-vous pour discuter avec d'autres utilisateurs.</Typography>
                            <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', gap: 3, mt: 2 }}>
                                <Button component={Link} to="/connexion" variant="contained" color="primary" sx={{ fontSize: '20px', fontWeight: 'bold', boxShadow: 2 }}>
                                    Se connecter
                                </Button>
                                <Button component={Link} to="/inscription" variant="contained" color="secondary" sx={{ fontSize: '20px', fontWeight: 'bold', boxShadow: 2 }}>
                                    S'inscrire
                                </Button>
                            </Box>
                        </>
                    )}
                </Box>
            </Box>

            <Footer />
        </>
    );
}
