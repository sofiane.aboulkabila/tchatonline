import { useState, useEffect, useRef } from 'react';
import { Typography, Button, TextField, Box, Switch, MenuItem, Select, Modal, useMediaQuery, useTheme } from '@mui/material';
import { useAuth } from '../../hooks/Auth/useAuth';
import io from 'socket.io-client';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import axios from 'axios';
import { Link } from 'react-router-dom';


export const TchatPage = () => {
    const { auth, authHeader, isAuthenticated } = useAuth();
    const [messages, setMessages] = useState([]);
    const [newMessage, setNewMessage] = useState('');
    const [socket, setSocket] = useState(null);
    const [connectedUsers, setConnectedUsers] = useState([]);
    const [currentRoom, setCurrentRoom] = useState('general');
    const [scrollEnabled, setScrollEnabled] = useState(true);
    const messagesEndRef = useRef(null);
    const newSocket = useRef(null);
    const [chatRooms, setChatRooms] = useState([]);


    const theme = useTheme();

    const isSmallScreen = useMediaQuery(theme.breakpoints.down('sm'));

    const defaultUsername = isAuthenticated ? auth.username : '';

    let defaultColor;

    if (isAuthenticated && auth.color) {
        defaultColor = auth.color
    }

    useEffect(() => {
        newSocket.current = io('http://localhost:8000', {
            extraHeaders: {
                Authorization: authHeader
            }
        });

        const fetchChatRooms = async () => {
            try {
                const response = await axios.get('http://localhost:8000/api/chatrooms', {
                    headers: {
                        Authorization: authHeader
                    }
                });
                const roomNames = response.data.map(room => room.name);
                setChatRooms(roomNames);
            } catch (error) {
                console.error(error);
            }

        };
        fetchChatRooms();
    }, [defaultUsername, authHeader]);

    useEffect(() => {
        if (newSocket.current && defaultUsername) {
            newSocket.current.on('connect', () => {
                newSocket.current.emit('send-nickname', defaultUsername, defaultColor, auth._id);
            });

            newSocket.current.on('receive_message', (message) => {
                setMessages(prevMessages => [...prevMessages, message]);

                const messageData = {
                    content: message.content,
                    author: message.senderId
                };

                axios.post('http://localhost:8000/api/messages', messageData, {
                    headers: {
                        Authorization: authHeader
                    }
                })
                    .then(response => {
                        console.log('Message enregistré avec succès:', response.data);
                    })
                    .catch(error => {
                        console.error('Erreur lors de l\'enregistrement du message: ', error);
                    });
            });


            newSocket.current.on('connected_users', (users) => {
                const usersObject = {};
                users.forEach(user => {
                    usersObject[user.socketId] = user.username;
                });
                setConnectedUsers(usersObject);
            });


            setSocket(newSocket.current);

            return () => {
                newSocket.current.off('connect');
                newSocket.current.off('receive_message');
                newSocket.current.off('connected_users');
                newSocket.current.disconnect();
            };
        }
    }, [defaultUsername, defaultColor, auth._id, authHeader]);


    useEffect(() => {
        if (scrollEnabled) {
            messagesEndRef.current?.scrollIntoView({ behavior: 'smooth' });
        }
    }, [messages, scrollEnabled]);

    const toggleScroll = () => setScrollEnabled(prev => !prev);

    const sendMessage = () => {
        if (isMessageValid()) {
            const isPrivate = isNewMessagePrivate();
            const recipient = isPrivate ? extractRecipient() : null;
            const messageContent = extractMessageContent();

            if (isPrivate) {
                sendPrivateMessage(messageContent, recipient);
            } else {
                sendMessageToRoom(messageContent);
            }

            resetNewMessage();
        }
    };


    const isMessageValid = () => newMessage.trim() !== '';
    const isNewMessagePrivate = () => newMessage.startsWith('@');
    const extractRecipient = () => newMessage.split(' ')[0].substring(1);
    const extractMessageContent = () => isNewMessagePrivate() ? newMessage.split(' ').slice(1).join(' ') : newMessage;

    const sendPrivateMessage = (content, recipient) => {
        socket.emit('send_private_message', { content, recipient });
    };

    const sendMessageToRoom = (content) => {
        const messageData = {
            content: content,
            author: auth._id,
            room: currentRoom
        };
        console.log(currentRoom)

        socket.emit('send_message', messageData);

        axios.post('http://localhost:8000/api/messages', messageData, {
            headers: { Authorization: authHeader }
        })
            .catch(error => {
                console.error('Error sending message:', error);
            });
    };


    const resetNewMessage = () => setNewMessage('');

    const joinRoom = (roomName) => {
        if (currentRoom !== roomName) {
            socket.emit('join_room', roomName);

            setCurrentRoom(roomName);
        }
    };

    const openPrivateChat = (recipient) => setNewMessage(`@${recipient} `);

    const renderMessages = () => messages.map((message, index) => {
        const isCurrentUser = message.sender === auth.username;
        const messageBackgroundColor = isCurrentUser ? '#2196f3' : '#e0e0e0';
        const messageColor = isCurrentUser ? '#ffffff' : '#000000';

        const usernameColor = message.color ? message.color : messageColor;

        return (
            <Box key={index} display="flex" flexDirection="column" alignItems={isCurrentUser ? 'flex-end' : 'flex-start'} mb={1} mt={2}>
                <Box bgcolor={messageBackgroundColor} color={messageColor} p={1} borderRadius={5} boxShadow={3} maxWidth="70%" sx={{ paddingLeft: 2, paddingRight: 2 }}>
                    {message.private && (
                        <Box sx={{ width: 'fit-content', backgroundColor: 'red', color: 'white', fontWeight: 'bold', padding: '3px 6px', marginLeft: 'auto', borderRadius: '15px' }}>
                            Message privé
                        </Box>
                    )}
                    <Box sx={{ display: 'flex' }}>
                        <AccountCircleIcon sx={{ fontSize: 32, cursor: 'pointer' }} onClick={() => openPrivateChat(message.sender)} />
                        <Box sx={{ display: 'inline-block', borderRadius: 5, p: 1, bgcolor: messageBackgroundColor }}>
                            <Typography variant="caption" sx={{ fontWeight: 'bold', color: usernameColor }}>
                                {message.private ? `@${message.sender}` : message.sender}
                            </Typography>
                        </Box>
                    </Box>
                    <Typography variant="body1">{message.content}</Typography>
                </Box>
            </Box>
        );
    });


    const renderLoggedInContent = () => (
        <Box p={2} sx={{ display: 'flex', alignItems: 'center', gap: 5, padding: 2 }}>
            <TextField fullWidth value={newMessage} onChange={(e) => setNewMessage(e.target.value)} variant="outlined" sx={{ background: 'white', border: '1px solid black' }} />
            <Button onClick={sendMessage} variant="contained" sx={{ borderRadius: 0, padding: '5px 20px', marginTop: '8px', marginRight: '30px' }}>Envoyer</Button>
        </Box>
    );

    const renderConnectedUsers = () => (
        <Box p={2} mt="auto">
            <Typography variant="h6">Utilisateurs connectés:</Typography>
            <ul style={{ listStyleType: 'none', padding: '10px 5px', width: 'fit-content' }}>
                {Object.values(connectedUsers).map((username, index) => (
                    <li key={index} style={{
                        backgroundColor: 'blue',
                        color: 'white',
                        borderRadius: 5,
                        padding: '4px',
                        display: 'inline-block',
                        marginRight: '5px'
                    }}>
                        {username}
                    </li>
                ))}
            </ul>
        </Box >
    );


    return (
        <Box display="flex" flexDirection="row" height="90vh" sx={{ background: '#403e3e2e', margin: 2, borderRadius: '20px' }}>
            {!isAuthenticated ? (
                <Modal
                    open={!isAuthenticated}
                    aria-labelledby="modal-title"
                    aria-describedby="modal-description"
                    style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                >
                    <Box
                        sx={{
                            position: 'absolute',
                            top: '50%',
                            left: '50%',
                            transform: 'translate(-50%, -50%)',
                            bgcolor: 'background.paper',
                            boxShadow: 24,
                            p: 4,
                            borderRadius: '10px',
                            textAlign: 'center',
                            width: 'fit-content',
                            height: 'fit-content'
                        }}
                    >
                        <Typography id="modal-title" variant="h6" component="h2">
                            Vous devez être connecté pour accéder à cette fonctionnalité
                        </Typography>
                        <Typography id="modal-description" sx={{ mt: 2 }}>
                            Veuillez vous connecter pour continuer.
                        </Typography>
                        <Button variant="contained" component={Link} to="/" color='error' sx={{ mt: 2, mr: 3 }}>Retourner à l'accueil</Button>
                        <Button variant="contained" component={Link} to="/connexion" sx={{ mt: 2 }}>Se connecter</Button>
                    </Box>
                </Modal>
            ) : (
                <Box display="flex" flexDirection="column" flexGrow={1}>
                    <Box position="relative" zIndex={1} p={3}>
                        <Box position="absolute" right="10px" top="10px" transform="translate(-50%, 0)">
                            <Select
                                value={currentRoom}
                                onChange={(e) => joinRoom(e.target.value)}
                                variant="outlined"
                                sx={{
                                    bgcolor: 'white',
                                    color: 'black', mr: 3
                                }}
                            >

                                {chatRooms.map((roomName, index) => (
                                    <MenuItem key={index} value={roomName}>{roomName}</MenuItem>
                                ))}
                            </Select>
                        </Box>
                    </Box>

                    <Box position="absolute" right="30px" bottom="30px" display="flex" alignItems="center" padding="8px 20px 8px 10px" borderRadius="20px" backgroundColor="#EEEEEE">
                        <Switch
                            checked={scrollEnabled}
                            onChange={toggleScroll}
                            color="default"
                            sx={{
                                '& .MuiSwitch-track': {
                                    backgroundColor: scrollEnabled ? '#4CAF50' : '#F44336',
                                },
                                '& .MuiSwitch-thumb': {
                                    backgroundColor: '#FFFFFF',
                                }
                            }}
                        />
                        {isSmallScreen ? null : (
                            <Typography
                                variant="body2"
                                sx={{
                                    color: scrollEnabled ? '#4CAF50' : '#F44336',
                                    fontSize: '1.1em',
                                    fontWeight: 'bold'
                                }}
                            >
                                {scrollEnabled ? 'Scroll automatique activé' : 'Scroll automatique désactivé'}
                            </Typography>
                        )}                    </Box>
                    <Box flexGrow={1} p={2} style={{ overflowY: 'auto' }}>
                        {renderMessages()}
                        <div ref={messagesEndRef} />
                    </Box>
                    {renderLoggedInContent()}
                    {renderConnectedUsers()}
                </Box>
            )}

        </Box >
    );
};