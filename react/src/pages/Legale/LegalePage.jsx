import { Container, Typography } from '@mui/material';

export const LegalePage = () => {
    return (
        <Container style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
            <div>
                <Typography variant="h3" gutterBottom>
                    Page mentions légales
                </Typography>
                <Typography variant="body1">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec consectetur libero. Nullam sed dui libero. Donec dictum consectetur justo, sit amet cursus felis luctus vel. Donec scelerisque nunc non lorem efficitur faucibus. Fusce nec mi a orci eleifend eleifend. Vivamus nec lectus at sapien elementum volutpat vel eu ligula. Cras non nisi eget urna efficitur interdum. Donec vel magna nec urna blandit vehicula. Nam sed metus rutrum, accumsan lorem et, faucibus neque. Pellentesque nec libero sit amet nisi venenatis egestas sit amet id risus. Mauris feugiat dui vel nulla consectetur, vel sollicitudin orci tristique. Phasellus ac velit nec ligula malesuada malesuada. Sed dapibus, ipsum eu feugiat posuere, lorem purus lacinia quam, non vulputate lorem tortor at libero. Donec consequat feugiat sapien, sed feugiat libero rutrum eget.
                </Typography>
            </div>
        </Container>
    );
};
