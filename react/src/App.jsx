import { Route, BrowserRouter, Routes } from 'react-router-dom';
import AuthProvider from 'react-auth-kit';
import createStore from 'react-auth-kit/createStore';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';

import { Navbar } from './components/Navbar/Navbar';
import { RgpdPage } from './pages/Rgpd/RgpdPage';
import { LegalePage } from './pages/Legale/LegalePage';
import { TchatPage } from './pages/Tchat/TchatPage';
import { ShopPage } from './pages/Shop/ShopPage';
import { HomePage } from './pages/Home/HomePage';
import { LoginPage } from './pages/Auth/LoginPage';
import { AdminUsersPage } from './pages/Admin/AdminUsersPage'
import { AdminMessagesPage } from './pages/Admin/AdminMessagesPage'
import { RegisterPage } from './pages/Auth/RegisterPage';
import { PageNotFound } from './pages/Errors/PageNotFound';
import { ProfilPage } from './pages/User/ProfilPage';
import { NotRequireAuth } from './middlewares/NotRequireAuth';
import { RequireAuth } from './middlewares/RequireAuth';
import { RequireAdmin } from './middlewares/RequireAdmin'

export const App = () => {
  return (
    <AuthProvider store={createStore({
      authName: '_auth',
      authType: 'cookie',
      cookieDomain: window.location.hostname,
      cookieSecure: false,
    })}>
      <BrowserRouter>

        <Navbar />

        <ToastContainer
          position="bottom-right"
          autoClose={3000}
          hideProgressBar={false}
          newestOnTop={true}
          closeOnClick={false}
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />

        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/boutique" element={<ShopPage />} />
          <Route path="/tchat" element={<TchatPage />} />
          <Route
            path="/inscription"
            element={
              <NotRequireAuth fallbackPath="/utilisateur/profil">
                <RegisterPage />
              </NotRequireAuth>
            }
          />
          <Route path="/mentions-legales" element={<LegalePage />} />
          <Route path="/rgpd" element={<RgpdPage />} />
          <Route
            path="/connexion"
            element={
              <NotRequireAuth fallbackPath="/utilisateur/profil">
                <LoginPage />
              </NotRequireAuth>
            }
          />
          <Route
            path="/utilisateur/profil"
            element={
              <RequireAuth fallbackPath="/connexion">
                <ProfilPage />
              </RequireAuth>
            }
          />

          <Route
            path="/admin/utilisateurs"
            element={
              <RequireAdmin fallbackPath="/utilisateur/profil">
                <AdminUsersPage />
              </RequireAdmin>
            }
          />


          <Route
            path="/admin/messages"
            element={
              <RequireAdmin fallbackPath="/utilisateur/profil">
                <AdminMessagesPage />
              </RequireAdmin>
            }
          />
          <Route path="*" element={<PageNotFound />} />
        </Routes>
      </BrowserRouter>
    </AuthProvider>
  );
};
