import useSignIn from 'react-auth-kit/hooks/useSignIn';
import secureLocalStorage from 'react-secure-storage';
import axios from 'axios';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';

export const useLogin = () => {
    const signIn = useSignIn();
    const navigate = useNavigate();

    return async (values, setSubmitting) => {
        try {
            const response = await axios.post('http://localhost:8000/api/login', values);

            if (values.rememberMe) {
                secureLocalStorage.setItem('username', values.username);
                secureLocalStorage.setItem('password', values.password);
                secureLocalStorage.setItem('rememberMe', 'true');
            } else {
                secureLocalStorage.removeItem('username');
                secureLocalStorage.removeItem('password');
                secureLocalStorage.setItem('rememberMe', 'false');
            }

            if (signIn({
                auth: {
                    token: response.data.token,
                    type: response.data.tokenType
                },
                userState: {
                    _id: response.data._id,
                    email: response.data.email,
                    first_name: response.data.first_name,
                    last_name: response.data.last_name,
                    role: response.data.role,
                    username: response.data.username,
                    color: response.data.color
                }
            })) {
                toast.success("Connexion réussie !");
                if (response.data.role === "Administrateur") {
                    navigate('/admin/utilisateurs');
                } else {
                    navigate('/utilisateur/profil');
                }
            }
        } catch (error) {
            if (error.response) {
                toast.error(error.response.data.message);
            } else if (error.request) {
                toast.error("Problème de réponse du serveur");
            } else if (error.request.data.message) {
                toast.error(error.response.data.message);
            } else {
                toast.error("Une erreur s'est produite lors de la connexion");
            }
        } finally {
            setSubmitting(false);
        }
    };
};