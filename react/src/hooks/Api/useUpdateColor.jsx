import axios from 'axios';
import { toast } from 'react-toastify';
import { useAuth } from '../Auth/useAuth';
import { useNavigate } from 'react-router-dom';

export const useUpdateColor = () => {
    const { auth, authHeader, signOut } = useAuth();
    const navigate = useNavigate();

    return async (values, setSubmitting) => {
        try {
            const response = await axios.put(`http://localhost:8000/api/users/edit/color/${auth._id}`, values, {
                headers: {
                    Authorization: authHeader
                }
            });

            if (response.status === 200) {
                toast.success("Changement de couleur de pseudo réussi. Veuillez vous reconnecter.");
                signOut();
                navigate('/connexion');
            } else {
                throw new Error("Failed to update color");
            }
        } catch (error) {
            if (error.response) {
                toast.error(error.response.data.message);
            } else if (error.request) {
                toast.error("Problème de réponse du serveur");
            } else if (error.request.data.message) {
                toast.error(error.response.data.message);
            } else {
                toast.error("Une erreur s'est produite lors de la connexion");
            }
        } finally {
            setSubmitting(false);
        }
    };
};
