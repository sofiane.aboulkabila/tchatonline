import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

export const useRegister = () => {
    const navigate = useNavigate();

    return async (values, setSubmitting) => {
        try {
            await axios.post('http://localhost:8000/api/register', values);

            toast.success("Inscription réussie !");
            navigate('/connexion');
        } catch (error) {
            console.error('Registration error:', error);
        } finally {
            setSubmitting(false);
        }
    };
};
