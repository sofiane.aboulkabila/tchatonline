import axios from 'axios';
import { toast } from 'react-toastify';
import { useAuth } from '../Auth/useAuth';
import { useNavigate } from 'react-router-dom';

export const useUpdatePassword = () => {
    const { auth, authHeader, signOut } = useAuth();
    const navigate = useNavigate();

    return async (values, setSubmitting) => {
        try {
            const response = await axios.put(`http://localhost:8000/api/users/edit/password/${auth._id}`, values, {
                headers: {
                    Authorization: authHeader
                }
            });

            if (response.status === 200) {
                toast.success("Changement de mot de passe réussi. Veuillez vous reconnecter.");
                signOut();
                navigate('/connexion');
            } else {
                throw new Error("Failed to update password");
            }
        } catch (error) {
            if (error.response) {
                toast.error(error.response.data.message);
            } else if (error.request) {
                toast.error("Problème de réponse du serveur");
            } else if (error.request.data.message) {
                toast.error(error.response.data.message);
            } else {
                toast.error("Une erreur s'est produite lors de la connexion");
            }
        } finally {
            setSubmitting(false);
        }
    };
};
