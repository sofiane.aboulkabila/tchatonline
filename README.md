##  Back-end:
Naviger dans (à la racine) api avec la commande: "cd api"
Puis installez les dépendences avec "npm install"
Ensuite pour démarrer le serveur "npm run start"
Lancer les tests unitaires de routes "npm test"

## Frontend:
Naviger dans (à la racine) react avec la commande: "cd react"
Puis installez les dépendences avec "npm install"
Ensuite pour démarrer le serveur "npm run dev"

